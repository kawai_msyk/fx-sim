import datetime
import sys
import logging
import glob
import csv
import os
from azure.cosmosdb.table.tableservice import TableService
from azure.cosmosdb.table.models import Entity
import ccy

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


path = "C:\\src\\fx01\\rate\\1m\\" # use your path
all_files = glob.glob(path + "2022063*.txt")

STORAGE_NAME = 'storageaccountrg03b52e'
STORAGE_KEY = 'cIoDGio95MAFBWCypz8ZZjRWNSItD4D6NgvouTfrPB+3lAmbu3WWFmnmXZcJkBfdBNnc1Nj54W4THls1+vs0bg=='

table_service = TableService(account_name=STORAGE_NAME, account_key=STORAGE_KEY)

def insert_files():
    li = []

    for filename in all_files:
        logger.debug(filename)
        insert(filename)



def insert(file):
    records = []
    with open(file) as f:
        reader = csv.reader(f)
        header = next(reader)
        rate_list = []
        cnt =  0
        before_pk = ""
        for r in csv.reader(f):
            #print(r)
            #<TICKER>,<DTYYYYMMDD>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>
            if r[0] not in ccy.CCY:
                continue

            #if cnt != 0 and before_pk != r[0]:
            #    with table_service.batch('rate1m') as batch:
            #        for r in rate_list:
            #            batch.insert_or_replace_entity(r)
            #    cnt = 0
            #    rate_list = []



            rate = Entity()
            rate.PartitionKey = r[0]  # 必須のキー情報
            rate.RowKey = str(datetime.datetime(int(r[1][0:4]), int( r[1][4:6]), int(r[1][6:8]) ,
                                            int(r[2][0:2]), int( r[2][2:4]), tzinfo=datetime.timezone.utc) )# 必須のキー情報
            rate.Open = float(r[3])
            rate.High = float(r[4])
            rate.Low = float(r[5])
            rate.Close = float(r[6])

            rate_list.append(rate)
            cnt = cnt + 1

            before_pk = r[0]
            print(rate)
            table_service.insert_or_replace_entity('rate1m', rate)
            #if cnt == 100:
            #    with table_service.batch('rate1m') as batch:
            #        for r in rate_list:
            #            batch.insert_or_replace_entity(r)
            #    cnt = 0
            #    rate_list = []




        #with table_service.batch('rate1m') as batch:
        #    for r in rate_list:
        #        batch.insert_or_replace_entity(r)

    #try:
    #    cur.executemany("INSERT INTO rate1m VALUES (%s, %s, %s, %s, %s, %s, %s)", records)
    #except:
    #    conn.rollback()

    #conn.commit()


if __name__ == '__main__':

    #print( ccy.CCY )
    r = insert_files()
