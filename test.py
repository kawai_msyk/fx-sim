import datetime
import time

current = datetime.datetime.now(datetime.timezone.utc)
print(current)
print(time.mktime(current.timetuple() ))

before = current  - datetime.timedelta(hours=24) #24時間前
print(before)
print(before.year)
b = before
start = datetime.datetime(b.year, b.month, b.day, 23, 0, 0, 0, tzinfo=datetime.timezone.utc)

print(start)
print(start.timetuple())
print(time.mktime(start.timetuple() ))
#datetime.datetime(2020, 1, 1, 10, 25, 37, 629103)

print(datetime.datetime.fromtimestamp(1614481734))