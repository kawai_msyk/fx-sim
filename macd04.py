import numpy as np
import pandas as pd
import techlib as tl
import sys
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


SHORT_TERM = 12
LONG_TERM = 26
SIGNAL_TERM = 9
LossCutPips = 30

RATEFILE = pd.read_csv('quote.csv',sep=',', \
    #names=('DATE','USD','GBP','EUR','CAD','CHF','AUD','NZD','ZAR'), \
    dtype=[('DATE','S10'), \
            ('USD','f8'),\
            ('GBP','f8'),\
            ('EUR','f8'),\
            ('CAD','f8'),\
            ('CHF','f8'),\
            ('AUD','f8'),\
            ('NZD','f8'),\
            ('ZAR','f8'),\
            ('ALL','f8'),\
            ('JPY','f8')], \
    parse_dates=True)

def initrate(ccy1, ccy2):
    rate = pd.DataFrame( {'DATE': [], 'CLOSE':[], 'AVG':[], 'MIN':[], 'MAX':[] })
    rate['CLOSE'] = RATEFILE[ccy1]/RATEFILE[ccy2]
    rate['DATE'] = RATEFILE['DATE']
    rate['AVG'] = rate['CLOSE'].rolling(250).mean()
    rate['MIN'] = rate['CLOSE'].rolling(20).mean()
    rate['MAX'] = rate['CLOSE'].rolling(20).mean()


    #rate = pd.read_csv('zarjpy.csv',sep=',', names=('DATE','CLOSE'), parse_dates=True)

    #macd = pd.DataFrame({'MACD':iMA(rate, 12) - iMA(rate, 26)})
    #signal = pd.DataFrame({'SIGNAL':iMA(macd,9,applied_price='MACD')})

    macd = tl.iMA(rate, SHORT_TERM) - tl.iMA(rate, LONG_TERM)
    macddf = pd.DataFrame({'MACD':macd})

    signal = tl.iMA( macddf, SIGNAL_TERM, applied_price='MACD')

    rsi = tl.rsi(rate, 14)
    #signal = pd.DataFrame({'SIGNAL':iMA(macd,9,applied_price='MACD')})
    macdset = pd.DataFrame({'MACD': macd, 'SIGNAL': signal, 'RSI':rsi})

    return rate, macdset, signal

def BuyEntry(i, macdset): # 買いシグナル
	if macdset['MACD'][i] >0.0 and  macdset['MACD'][i-1] < macdset['SIGNAL'][i-1] \
   	and macdset['MACD'][i] > macdset['SIGNAL'][i]:
        
		return True
    

def SellEntry(i, macdset): # 売りシグナル
	if macdset['MACD'][i] < 0.0 and  macdset['MACD'][i-1] > macdset['SIGNAL'][i-1] \
    and macdset['MACD'][i] < macdset['SIGNAL'][i]:
        
		return True
    
    

def BuyExit(i, p, rate, macdset): # 買いポジション決済シグナル
    #if macdset['MACD'][i-1] > macdset['SIGNAL'][i-1] \
    #and macdset['MACD'][i] < macdset['SIGNAL'][i]:
    #    return True
    
    if rate['CLOSE'][i] - p < -LossCutPips*0.01:
        return True, p-LossCutPips*0.01
        #return True, rate['CLOSE'][i]

    if SellEntry(i, macdset):
        return True, rate['CLOSE'][i]
    #if rate['CLOSE'][i]/LongPos >1.05:
    #    return True


    return False, 0.0

def SellExit(i, p, rate, macdset): # 売りポジション決済シグナル
    #if macdset['MACD'][i-1] < macdset['SIGNAL'][i-1] \
   	#and macdset['MACD'][i] > macdset['SIGNAL'][i]:
    #    return True
    
    if rate['CLOSE'][i] - p > LossCutPips*0.01:
        return True, p + LossCutPips*0.01
        #return True, rate['CLOSE'][i]

    if BuyEntry(i, macdset):
        return True, rate['CLOSE'][i]

    #if rate['CLOSE'][i]/ShortPos <0.95:
    #    return True

    return False, 0.0

#LongPos = pd.Series(0.0, index=rate.index) # 買いポジション情報
#ShortPos = LongPos.copy() # 売りポジション情報
Lots = 1.0 # 売買ロット数

#LongPos = pd.DataFrame(colums['DATE','RATE','PROFIT'])
#ShortPos = pd.DataFrame()

def sim(rate, macdset):
    LongPos = 0.0
    ShortPos =0.0
    TotalProfit =0.0
    PROFIT = 0.0
    for i in range( 60, len(rate)-1 ):
        if LongPos != 0.0 and BuyExit(i, LongPos, rate, macdset)[0]:
            #PROFIT = rate['CLOSE'][i] - LongPos
            PROFIT = BuyExit(i, LongPos, rate, macdset)[1] - LongPos
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE BUY {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0

        if ShortPos != 0.0 and  SellExit(i, ShortPos, rate, macdset)[0]: 
            PROFIT = SellExit(i, ShortPos, rate, macdset)[1]  - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE SELL {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
        
        if BuyEntry(i, macdset) and LongPos == 0.0: 
            LongPos = rate['CLOSE'][i]
            #if ShortPos!= 0.0:
            #    PROFIT = rate['CLOSE'][i] - ShortPos
            #    PROFIT = -PROFIT
            #    TotalProfit = TotalProfit + PROFIT
            #    logger.debug("Buy {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            #    ShortPos = 0.0
                
        if SellEntry(i, macdset) and ShortPos == 0.0: 
            ShortPos = rate['CLOSE'][i]
            #if LongPos!= 0.0:
            #    PROFIT =  rate['CLOSE'][i] - LongPos
            #    TotalProfit = TotalProfit + PROFIT
            #    logger.debug("Sell {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], LongPos, PROFIT) )
            #    LongPos = 0.0

    log =ccy1+" "+ccy2+" "+"{:.4f}".format(TotalProfit)
    logger.info(log)
    #print(TotalProfit)
    #print(LongPos)
    #print(ShortPos)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        ccy1=sys.argv[1]
        ccy2=sys.argv[2]
        if ccy2!='JPY':
            LossCutPips = LossCutPips/100

        r, m, s = initrate(ccy1, ccy2)
        sim(r,m)

    else:    
        CCY = ["JPY", "USD", "GBP", "EUR", "AUD", "NZD", "CAD", "CHF", "ZAR"]

        for i in range(9):
            for j in range(9):
                LossCutPips = 30
                ccy1 = CCY[j]
                ccy2 = CCY[i]
                if ccy1 == ccy2:
                    continue
                if ccy1 == "GBP" or ccy2 == "GBP":
                    LossCutPips = 30
                if ccy2!='JPY':
                    LossCutPips = LossCutPips/100
                r, m, s = initrate(ccy1, ccy2)
                sim(r,m)