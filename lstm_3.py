import pandas as pd
import datetime as dt
import numpy as np
import pandas_datareader.data as web
import matplotlib
import matplotlib.pylab as plt

start = dt.date(2016,1,1)
end = dt.date(2017,9,20)
df= web.DataReader('AMZN',"yahoo",start,end)

df['Diff'] = 0.0
df['Std'] = 0.0

for i in range(1, len(df)):
    #df['Diff'][i] = df['Close'][i] - df['Close'][i-1]
    df.iloc[i,6] = df.iloc[i, 3] / df.iloc[i-1, 3]
    if i>25:
        df.iloc[i,7] = df.iloc[i-25:i, 3].std()


print(df.head(5))

#plt.plot(df['Close'])
#plt.show()

from sklearn.preprocessing import MinMaxScaler
 
scaler = MinMaxScaler(feature_range=(0, 1))
#df['Diff'] = scaler.fit_transform(df['Diff'])
 
#訓練データと教師データへの分割
 
x,t = [],[]
 
N = len(df)-1
M = 11
for n in range(25,N):
  _x = df['Diff'][n-M:n]
  #_x = df.iloc[n-M:n,6]
  std= df['Std'][n-M:n]
  _t = df['Diff'][n]
  x.append(_x+std)
  #x.append(std)
  t.append(_t)
 
x = np.array(x, dtype = np.float32)
x = scaler.fit_transform(x)
t = np.array(t, dtype = np.float32).reshape(len(t),1)
#print(x)

x = scaler.fit_transform(x)

import chainer
import chainer.links as L
import chainer.functions as F
from chainer import Chain, Variable, datasets, optimizers
from chainer import report, training
from chainer.training import extensions
import chainer.cuda

# 訓練：60%, 検証：40%で分割する
n_train = int(len(x) * 0.6)
dataset = list(zip(x, t))
train, test = chainer.datasets.split_dataset(dataset, n_train)


 
# ニューラルネットワークモデルを作成
class RNN(Chain):
    def __init__(self, n_units, n_output):
        super().__init__()
        with self.init_scope():
            self.l1 = L.Linear(None, n_units)
            self.l2 = L.Linear(None, n_units)
            self.l3 = L.Linear(None, n_output)
        
    def reset_state(self):
        #self.l1.reset_state()
        return
        
    def __call__(self, x, t):
        y = self.predict(x)
        loss = F.mean_squared_error(y, t)
        report({'loss':loss},self)
        return loss
        
    def predict(self, x):
        #if train:
        #    h1 = F.dropout(self.l1(x),ratio = 0.5)
        #else:
        h1 = self.l1(x)
        h2 = self.l2(x)
        return self.l3(h2)
 
## LSTMUpdaterを作る。
class LSTMUpdater(training.StandardUpdater):
    def __init__(self, data_iter, optimizer, device=None):
        super(LSTMUpdater,self).__init__(data_iter, optimizer, device=None)
        self.device = device
        
    def update_core(self):
        data_iter = self.get_iterator("main")
        optimizer = self.get_optimizer("main")
        
        batch = data_iter.__next__()
        x_batch, t_batch = chainer.dataset.concat_examples(batch, self.device)
        
        optimizer.target.reset_state()           #追加
        optimizer.target.cleargrads()
        loss = optimizer.target(x_batch, t_batch)
        loss.backward()
        loss.unchain_backward()                  #追記
        optimizer.update() 


        np.random.seed(1)
 
# モデルの宣言
model = RNN(50, 1)
 
# GPU対応
#chainer.cuda.get_device(0).use()
#model.to_gpu()                 
 
# Optimizer
optimizer = optimizers.Adam()
optimizer.setup(model)
 
# Iterator
batchsize = 20
train_iter = chainer.iterators.SerialIterator(train, batchsize)
test_iter = chainer.iterators.SerialIterator(test, batchsize, repeat=False, shuffle=False)
 
# Updater &lt;- LSTM用にカスタマイズ
#updater = LSTMUpdater(train_iter, optimizer,device = -1)
updater = training.StandardUpdater(train_iter, optimizer)
 
# Trainerとそのextensions
epoch = 3000
trainer = training.Trainer(updater, (epoch, 'epoch'), out='result')
 
# 評価データで評価
trainer.extend(extensions.Evaluator(test_iter, model,device = -1))
 
# 学習結果の途中を表示する
trainer.extend(extensions.LogReport(trigger=(100, 'epoch')))
 
# １エポックごとに、trainデータに対するlossと、testデータに対するlossを出力させる
trainer.extend(extensions.PrintReport(['epoch', 'main/loss', 'validation/main/loss', 'elapsed_time']), trigger=(100, 'epoch'))

trainer.run()


# 結果を読み込み
import json
with open('result/log') as f:
     logs = json.load(f)
 
# 結果を整理
loss_train = [ log['main/loss'] for log in logs ]
loss_test  = [ log['validation/main/loss'] for log in logs ]
 
# プロット
plt.plot(loss_train, label='train')
plt.plot(loss_test,  label='test')
plt.legend()
plt.show()

n_train = int(len(x) * 0.6)
train_x, test_x = x[:n_train], x[n_train:]
train_t, test_t = t[:n_train], t[n_train:]
 
# 訓練データ
model.reset_state()
train_y = model.predict(Variable(train_x)).data# 訓練：60%, 検証：40%でランダムに分割する
train_x, test_x = x[:n_train], x[n_train:]
train_t, test_t = t[:n_train], t[n_train:].data
 
# プロット
plt.figure(figsize=(12,6))
 
 
plt.plot(train_y, color='red')   # 予測値
plt.plot(train_t, color='blue')  # 実測値
 
plt.show()




# 検証データ
model.reset_state()
test_y = model.predict(Variable(test_x)).data
 
# プロット
plt.figure(figsize=(12,6))
 
plt.plot(test_y, color='red')   # 予測値
plt.plot(test_t, color='blue')  # 実測値
 
plt.show()