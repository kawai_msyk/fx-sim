import requests
import time
import sqlite3

class RatePicker:
    CCY = {'1': 'USDJPY',
        '2': 'EURJPY',
        '3': 'GBPJPY',
        '4': 'AUDJPY',
        '5': 'NZDJPY',
        '6': 'CADJPY',
        '7': 'CHFJPY',
        '8': 'SGDJPY',
        '9': 'HKDJPY',
        'A': 'ZARJPY',
        'B': 'TRYJPY',
        'C': 'MXNJPY',
        'D': 'PLNJPY',
        'G': 'EURUSD',
        'H': 'GBPUSD',
        'I': 'AUDUSD',
        'J': 'NZDUSD',
        'L': 'EURGBP',
        'M': 'EURAUD',
        'N': 'GBPAUD',
    }

    def inser_db(self):
        dbname = 'TEST.db'
        conn = sqlite3.connect(dbname)



    def get_rate(self):
        current_time = str(int(time.time() * 1000)) 
        response = requests.get('https://www.fxprime.com/superx/ratelist.txt?_='+ current_time)

        #print(response.status_code)    # HTTPのステータスコード取得

        result = response.text.split('&')
        result_dic = {}
        for i in range(len(result)):
            k, v = result[i].split('=') 
            result_dic[k] = v

        #print(result_dic)
            

        for k in result_dic.keys():
            if k == 'year' or k == 'month' or k=='day' or k=='hour' or k=='minutes':
                continue

            if 'BID' not in k :
                continue

            n = k.split('_')[1]
            c = self.CCY[n]
            print(c +" " +  result_dic[k])


if __name__ == '__main__':
    rp = RatePicker()
    rp.get_rate()
