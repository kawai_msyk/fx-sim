import urllib.request
import mysql.connector as mydb
import glob
import csv
import os
import json

class MizuhoRate:
    Pair_List = ["01_USDJPY_D.csv","02_EURJPY_D.csv","03_EURUSD_D.csv","04_GBPJPY_D.csv","05_GBPUSD_D.csv","06_CADJPY_D.csv","07_USDCAD_D.csv","08_AUDJPY_D.csv","09_AUDUSD_D.csv","10_USDCHF_D.csv","11_NZDUSD_D.csv","12_EURGBP_D.csv","13_NZDJPY_D.csv","14_CHFJPY_D.csv","15_EURCHF_D.csv","16_ZARJPY_D.csv","18_GBPCHF_D.csv","20_AUDCHF_D.csv","21_AUDNZD_D.csv","22_NZDCHF_D.csv","23_CNHJPY_D.csv","24_EURAUD_D.csv","26_TRYJPY_D.csv","27_GBPAUD_D.csv","28_MXNJPY_D.csv"]
    #URL = "https://info.ctfx.jp/service/market/csv/"
    URL = "https://www.mizuhobank.co.jp/market/"
    CSV = "quote.csv"
    STORAGE_NAME = 'storageaccountrg03b52e'
    STORAGE_KEY = 'cIoDGio95MAFBWCypz8ZZjRWNSItD4D6NgvouTfrPB+3lAmbu3WWFmnmXZcJkBfdBNnc1Nj54W4THls1+vs0bg=='

    def download(self):
        #for csv in pairlist: 
        # ダウンロードする
        url = "https://www.mizuhobank.co.jp/market/quote.csv"
        headers = {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0"
        }

        request = urllib.request.Request(url, headers=headers) 
        mem = urllib.request.urlopen(request).read()
 
        # ファイルへの保存
        with open("/mnt/c/rate/mizuho_daily/quote.csv", mode="wb") as f:
            f.write(mem)


    def insert_db(self):
        conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx'
        )

        # コネクションが切れた時に再接続してくれるよう設定
        conn.ping(reconnect=True)

        # 接続できているかどうか確認
        print(conn.is_connected())
        cur = conn.cursor()

        cur.execute("Truncate table rate1d ")
        
        records = []
        with open("/mnt/c/rate/mizuho_daily/quote.csv", encoding="shift_jisx0213") as f:
            for r in csv.reader(f):
                records.append(r)

            records.pop(0) #ヘッダー削除
            records.pop(0)

            #print(records)

            for i in range(1,len(records[0])):
                insert_records = []
                if records[0][i] not in ("USD", "GBP","EUR","CAD","CHF","AUD","NZD","ZAR","HKD","SGD","MXN","TRY"):
                    continue

                ccy = records[0][i]+"JPY"

                for j in range(1,len(records)):
                    date = records[j][0].split("/")[0]
                    date += ("0" + records[j][0].split("/")[1])[-2:]
                    date += ("0" + records[j][0].split("/")[2])[-2:]
                    
                    time = "000000"
                    closing = records[j][i]
                    if closing == None or closing == '':
                        continue

                    insert_records.append([ccy,date,time,closing,closing,closing,closing])

 
                #print(insert_records)
                #<TICKER>,<DTYYYYMMDD>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>
                cur.executemany("INSERT INTO rate1d VALUES (%s, %s, %s, %s, %s, %s, %s)", insert_records)
                conn.commit()

    def get_ccy(self, cd):
        for pair in self.Pair_List:
            if cd == pair[0:2]:
                return pair[3:9]
        return None

    def make_cross(self):
        conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx'
        )

        cur = conn.cursor()

        pair = [("EURJPY", "USDJPY"),
                ("GBPJPY", "USDJPY"),
                ("AUDJPY", "USDJPY"),
                ("NZDJPY", "USDJPY"),
                ("USDJPY", "CHFJPY"),
                ("EURJPY", "GBPJPY"),
                ("EURJPY", "AUDJPY"),
                ("EURJPY", "NZDJPY"),
                ("EURJPY", "CHFJPY"),
                ("GBPJPY", "AUDJPY"),
                ("GBPJPY", "NZDJPY")
                ]

        for p in pair:
            #print(p[0],p[1])
            sql = "SELECT a.date, TRUNCATE(a.close/b.CLOSE,4) FROM rate1d as a " \
                    " inner JOIN rate1d as b on a.date = b.date " \
                    " where a.ccy= %s " \
                    " and b.ccy = %s "

            cur.execute(sql, p)
            rows = cur.fetchall()
            #print(rows)
            insert_records = []
            for i in range(len(rows)):
                ccy = p[0][0:3] + p[1][0:3]
                date = rows[i][0]
                time = "000000"
                closing = rows[i][1]

                insert_records.append([ccy,date,time,closing,closing,closing,closing])
            
            cur.executemany("INSERT INTO rate1d VALUES (%s, %s, %s, %s, %s, %s, %s)", insert_records)
            conn.commit()


    def get_json(self, ccy):
        conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx'
        )
        cur = conn.cursor()

        sql = "SELECT * FROM rate1d" \
                " where ccy= %s " \
                " order by date, time "

        cur.execute(sql, [ccy])
        rows = cur.fetchall()

        result_dic = {"CCY":ccy, "list":[]}
        for r in rows:
            detail = {"date":r[1], "time":r[2], "open":r[3], "high":r[4], "low":r[5] , "close":r[6]}

            result_dic["list"].append( detail )

        
        del result_dic["list"][:len(result_dic["list"])-500 ]
        #print(result_dic)
        return result_dic


    def get_ccy(self):
        ccylist = []
        f = open('CCYLIST.txt', 'r')
        
        for line in f:
            #line = f.readline()
            ccy = line.replace("\n", "")    
            ccylist.append(ccy)
        
        f.close
        
        #print(ccylist)
        return ccylist
        
    def insert_blob(self):
        ccy_list = self.get_ccy()

        print("[")
        for i,ccy in enumerate(ccy_list):
            if i < len(ccy_list)-1:
                print( json.dumps(self.get_json(ccy[0:6]) ), ",")
            else:
                print( json.dumps(self.get_json(ccy[0:6]) ))

        print("]")

if __name__ == '__main__':
    r =  MizuhoRate()
    r.download()
    r.insert_db()
    #print(r.get_ccy("01"))

    r.make_cross()

    #r.get_json("USDJPY")
    
    #r.insert_blob()
