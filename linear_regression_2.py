import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pylab as plt
import seaborn as sns


from sklearn import linear_model


def rsi( data, term):
    up = 0.0
    down = 0.0
    for i in range(0, term):
        if data[i] < data[i+1]:
            up += data[i+1] - data[i]
        else:
            down += data[i] - data[i+1]
    return up/(up+down)


data_dir = "./"
#レート読み込み　日付昇順
data = np.loadtxt(data_dir + "usdjpy.csv", delimiter=",",  usecols=(1) ) 

day_ago = 25
rsi_num = 11
rsi_term = 14

X = np.zeros((len(data), day_ago ))
#X = np.zeros((len(data), day_ago + rsi_num))
Y = np.zeros(len(data))

#for i in range(0, 1):
for i in range(0, len(data) - day_ago):
    for j in range(0, day_ago):
        X[i, j] = data[i+j]

#    for j in range(0, rsi_num):
#        X[i, j+day_ago] = rsi(data[i+j:i+j+rsi_term+1], rsi_term)


    Y[i] = data[i+day_ago]


X_train = X[0:3000]
Y_train = Y[0:3000]

X_test  = X[3001:3500]
Y_test  = Y[3001:3500]

np.savetxt("usdjpy_pre.csv", X_train, delimiter="," ,fmt='%.2f')

linear_reg_model = linear_model.LinearRegression(normalize=False) # モデルの定義

linear_reg_model.fit(X_train, Y_train) # モデルに対して、学習データをフィットさせ係数を学習させます


#X_test = np.array([[110.017,110.045,110.471,110.346,109.588,109.291,109.537,109.349,109.587,109.617,108.346,108.061,108.135,108.454,108.389,108.176,108.457,108.513,108.506,108.392,108.544,108.526,108.425,108.099,107.314]])

Y_pred = linear_reg_model.predict(X_test) # 検証データを用いて目的変数を予測
np.savetxt("usdjpy_pre_y_pred.csv", Y_pred, delimiter="," ,fmt='%.2f')
np.savetxt("usdjpy_pre_y_test.csv", Y_test, delimiter="," ,fmt='%.2f')


print(linear_reg_model.coef_)
#print(Y_pred)

plt.scatter(Y_pred, Y_pred - Y_test, color = 'blue')      # 残差をプロット 
plt.hlines(y = 0, xmin = -10, xmax = 50, color = 'black') # x軸に沿った直線をプロット
plt.title('Residual Plot')                                # 図のタイトル
plt.xlabel('Predicted Values')                            # x軸のラベル
plt.ylabel('Residuals')                                   # y軸のラベル
plt.grid()                                                # グリッド線を表示

plt.show()         

#np.savetxt("usdjpy_pre_y.csv", Y, delimiter="," ,fmt='%.2f')


'''
#data.head() # データの概要を見てみます
rate = np.array(data)
#print(data2)
 # 何日前までのデータを使用するのかを設定
num_sihyou = 1 # 終値



X = np.zeros((len(data2), day_ago*num_sihyou)) 


data_show = pd.DataFrame(X)
print(data_show)

Y = np.zeros(len(data2))

# 何日後を値段の差を予測するのか決めます
pre_day = 1
Y[0:len(Y)-pre_day] = X[pre_day:len(X),0] - X[0:len(X)-pre_day,0]
'''

