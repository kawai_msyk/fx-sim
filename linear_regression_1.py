import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pylab as plt
import seaborn as sns


from sklearn import linear_model # scikit-learnライブラリの関数を使用します


X_train = np.random.rand(100,2) # 0〜1の乱数で 100行2列の行列を生成
X_test = np.random.rand(100,2) # 0〜1の乱数で 100行2列の行列を生成

# 係数を設定
w1 = 1.0
w2 = 2.0
b = 3.0

# モデルからの誤差となるノイズを作成 
noise_train = 0.1*np.random.randn(100)
noise_test = 0.1*np.random.randn(100)

Y_train = w1*X_train[:,0] + w2*X_train[:,1]  + b + noise_train
Y_test = w1*X_test[:,0] + w2*X_test[:,1] + b + noise_test 

linear_reg_model = linear_model.LinearRegression() # モデルの定義

linear_reg_model.fit(X_train, Y_train) # モデルに対して、学習データをフィットさせ係数を学習させます

print("回帰式モデルの係数")
print(linear_reg_model.coef_) 
print(linear_reg_model.intercept_) 

Y_pred = linear_reg_model.predict(X_test) # テストデータから予測してみる

result = pd.DataFrame(Y_pred) # 予測
result.columns = ['Y_pred']
result['Y_test'] = Y_test
#sns.set_style('darkgrid')
sns.regplot(x='Y_pred', y='Y_test', data=result)
plt.show()