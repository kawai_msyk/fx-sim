import numpy as np
import pandas as pd
import techlib as tl

SHORT_TERM = 12
LONG_TERM = 26
SIGNAL_TERM = 9
STDEV_TERM = 7
STDEV_SIG_TERM = 3

r = pd.read_csv('quote.csv',sep=',', \
    #names=('DATE','USD','GBP','EUR','CAD','CHF','AUD','NZD','ZAR'), \
    dtype=[('DATE','S10'), \
            ('USD','f8'),\
            ('GBP','f8'),\
            ('EUR','f8'),\
            ('CAD','f8'),\
            ('CHF','f8'),\
            ('AUD','f8'),\
            ('NZD','f8'),\
            ('ZAR','f8'),\
            ('JPY','f8')], \
    parse_dates=True)

ccy1='ZAR'
ccy2='USD'

rate = pd.DataFrame( {'DATE': [], 'CLOSE':[], 'STDEV':[], 'DIFF':[], 'STDEVSIGNAL':[], 'MACD':[],'MACDSIGNAL':[]} )

rate['CLOSE'] = r[ccy1]/r[ccy2]
rate['DATE'] = r['DATE']



std = tl.stdev(rate, STDEV_TERM)
rate['STDEV'] = std

d=[]
diff = pd.DataFrame( {'DIFF':[]} )
for i in range( 0, len(rate)):
    if i < STDEV_TERM:
        d = d + [0]
    else:
        d = d+[std[i]-std[i-1]]

rate['DIFF'] = d

stdevsig = tl.iMA(rate, STDEV_SIG_TERM, applied_price='DIFF')
rate['STDEVSIGNAL'] = stdevsig


macd = tl.iMA(rate, SHORT_TERM) - tl.iMA(rate, LONG_TERM)
rate['MACD'] = macd

macddf = pd.DataFrame({'MACD':macd})

signal = tl.iMA( macddf, SIGNAL_TERM, applied_price='MACD',ma_method='SMA')
rate['MACDSIGNAL'] = signal


print(rate)

def BuyEntry(i): # 買いシグナル
    if  rate['DIFF'][i-1] < 0.0 \
    and rate['DIFF'][i] > 0.0 \
    and rate['MACD'][i] > rate['MACDSIGNAL'][i]:
        return True

def SellEntry(i): # 売りシグナル
    if  rate['DIFF'][i-1] < 0.0 \
    and rate['DIFF'][i] > 0.0 \
    and rate['MACD'][i] < rate['MACDSIGNAL'][i]:
        return True
    

def BuyExit(i): # 買いポジション決済シグナル
    if  rate['DIFF'][i-1] > 0.0  \
    and rate['DIFF'][i] < 0.0: 
        return True

def SellExit(i): # 売りポジション決済シグナル
    return BuyExit(i)
    

#LongPos = pd.Series(0.0, index=rate.index) # 買いポジション情報
#ShortPos = LongPos.copy() # 売りポジション情報
Lots = 1.0 # 売買ロット数

#LongPos = pd.DataFrame(colums['DATE','RATE','PROFIT'])
#ShortPos = pd.DataFrame()


LongPos = 0.0
ShortPos =0.0
TotalProfit =0.0
PROFIT = 0.0
for i in range( SHORT_TERM+LONG_TERM+SIGNAL_TERM+1, len(rate)-1 ):

    if BuyEntry(i) and LongPos == 0.0: 
       # LongPos[i+1] = Lots # 買いシグナル
       # LongPos.append( pd.DataFrame( {'DATE':rate['DATE'][i], 'RATE':rate['CLOSE']} ) )
        LongPos = rate['CLOSE'][i]
        '''
        if ShortPos!= 0.0:
            PROFIT = rate['CLOSE'][i] - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT
            print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
        ''' 
    elif SellEntry(i) and ShortPos == 0.0: 
        # LongPos[i+1] = Lots # 買いシグナル
        # LongPos.append( pd.DataFrame( {'DATE':rate['DATE'][i], 'RATE':rate['CLOSE']} ) )
        ShortPos = rate['CLOSE'][i]
        '''
        if LongPos!= 0.0:
            PROFIT =  rate['CLOSE'][i] - LongPos
            TotalProfit = TotalProfit + PROFIT
            print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0
        '''

    elif BuyExit(i) and LongPos != 0.0:
        PROFIT = rate['CLOSE'][i] - LongPos
        TotalProfit = TotalProfit + PROFIT
        print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], LongPos, PROFIT) )
        LongPos = 0.0

    elif SellExit(i) and ShortPos != 0.0: 
        PROFIT = rate['CLOSE'][i] - ShortPos
        PROFIT = -PROFIT
        TotalProfit = TotalProfit + PROFIT
        print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
        ShortPos = 0.0


print('total')
print(TotalProfit)
print(LongPos)
print(ShortPos)

