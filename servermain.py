import sys
import os

from flask import Flask
from flask_cors import CORS
from flask import jsonify
from flask import request
import mysql.connector as mydb
import csv
import json
import requests
import time

app = Flask(__name__)
CORS(app)

def get_connection():
    conn = mydb.connect(
        host='127.0.0.1',
        port='3306',
        user='root',
        password='root',
        database='fx'
    )
    return conn

@app.route('/rate/<dt>/<tm>')
def edit(dt ,tm):
    conn = get_connection()
    
    print(conn.is_connected())

    cur = conn.cursor()
    cur.execute("SELECT * FROM rate1d where  Date = %s and  Time = %s", (dt, tm ))
    rows = cur.fetchall()
    
    result = []
    for r in rows:
        line = {}
        line["ccy"] = r[0]
        line["date"] = r[1]
        line["time"] = r[2]
        line["open"] = r[3]
        line["high"] = r[4]
        line["low"] = r[5]
        line["close"] = r[6]

        result.append(line)
        print(line)

    return json.dumps(result)

@app.route('/rate/update', methods=["POST"])
def update():
    data = request.data.decode('utf-8')
    data = json.loads(data)

    d = data[0]["date"]
    t = data[0]["time"]

    print(d,t)
    conn = get_connection()
    cur = conn.cursor()
    cur.execute("delete from rate1d where Date=%s and Time=%s",(d, t))
    conn.commit()
    
    
    sql = "INSERT INTO rate1d VALUES (%s, %s, %s, %s, %s, %s, %s)"

    for r in data:
        cur.execute(sql, (r['ccy'], r['date'], r['time'], r['open'], r['high'], r['low'], r['close']) )
        print(r)  

    conn.commit()

    return "OK"


@app.route('/rate/get_fxprime_rate', methods=["GET"])
def get_fxprime_rate():
    CCY = {'1': 'USDJPY','2': 'EURJPY','3': 'GBPJPY','4': 'AUDJPY','5': 'NZDJPY','6': 'CADJPY','7': 'CHFJPY',
        '8': 'SGDJPY','9': 'HKDJPY','A': 'ZARJPY','B': 'TRYJPY','C': 'MXNJPY','D': 'PLNJPY','G': 'EURUSD',
        'H': 'GBPUSD','I': 'AUDUSD','J': 'NZDUSD','L': 'EURGBP','M': 'EURAUD','N': 'GBPAUD',}

    current_time = str(int(time.time() * 1000)) 
    response = requests.get('https://www.fxprime.com/superx/ratelist.txt?_='+ current_time)
    print(response.status_code)    # HTTPのステータスコード取得

    result = response.text.split('&')
    result_dic = {}
    for i in range(len(result)):
        k, v = result[i].split('=') 
        result_dic[k] = v

    print(result_dic)   
    
    y = result_dic['year']
    m = result_dic['month']
    d = result_dic['day']

    gbp = 0
    nzd = 0
    chf = 0
    eur = 0
    usd = 0
    result = []
    for k in result_dic.keys():
        if k == 'year' or k == 'month' or k=='day' or k=='hour' or k=='minutes':
            continue

        if 'BID' not in k :
            continue

        n = k.split('_')[1]
        c = CCY[n]
        line ={}
        line["ccy"] = c
        line["date"] = y + m + d
        line["time"] = "000000"
        line["open"] = result_dic[k]
        line["high"] = result_dic[k]
        line["low"] = result_dic[k]
        line["close"] = result_dic[k]
        
        result.append(line)

        if c== "GBPJPY":
            gbp = result_dic[k]
        
        if c == "NZDJPY":
            nzd = result_dic[k]

        if c== "EURJPY":
            eur = result_dic[k]
        
        if c == "CHFJPY":
            chf = result_dic[k]

        if c == "USDJPY":
            usd = result_dic[k]


    line = {}
    line["ccy"] = "GBPNZD"
    line["date"] = y + m + d
    line["time"] = "000000"
    line["open"] = '{:.5f}'.format(float(gbp)/float(nzd))
    line["high"] = '{:.5f}'.format(float(gbp)/float(nzd))
    line["low"] = '{:.5f}'.format(float(gbp)/float(nzd))
    line["close"] = '{:.5f}'.format(float(gbp)/float(nzd))
    result.append(line)
    

    line = {}
    line["ccy"] = "EURCHF"
    line["date"] = y + m + d
    line["time"] = "000000"
    line["open"] = '{:.5f}'.format(float(eur)/float(chf))
    line["high"] = '{:.5f}'.format(float(eur)/float(chf))
    line["low"] = '{:.5f}'.format(float(eur)/float(chf))
    line["close"] = '{:.5f}'.format(float(eur)/float(chf))
    result.append(line)

    line = {}
    line["ccy"] = "EURNZD"
    line["date"] = y + m + d
    line["time"] = "000000"
    line["open"] = '{:.5f}'.format(float(eur)/float(nzd))
    line["high"] = '{:.5f}'.format(float(eur)/float(nzd))
    line["low"] = '{:.5f}'.format(float(eur)/float(nzd))
    line["close"] = '{:.5f}'.format(float(eur)/float(nzd))
    result.append(line)

    line = {}
    line["ccy"] = "USDCHF"
    line["date"] = y + m + d
    line["time"] = "000000"
    line["open"] = '{:.5f}'.format(float(usd)/float(chf))
    line["high"] = '{:.5f}'.format(float(usd)/float(chf))
    line["low"] = '{:.5f}'.format(float(usd)/float(chf))
    line["close"] = '{:.5f}'.format(float(usd)/float(chf))
    result.append(line)

    print(result)
    return json.dumps(result)

if __name__ == '__main__':
    app.run('127.0.0.1', 8082, debug=True)