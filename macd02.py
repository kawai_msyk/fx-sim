import numpy as np
import pandas as pd


SHORT_TERM = 12
LONG_TERM = 26
SIGNAL_TERM = 9

def iMA(df, ma_period, ma_shift=0, ma_method='SMA', applied_price='CLOSE'):
    if ma_method == 'SMA':    
        return df[applied_price].rolling(ma_period).mean().shift(ma_shift)
    elif ma_method == 'EMA':
        return df[applied_price].ewm(span=ma_period).mean().shift(ma_shift)
    elif ma_method == 'SMMA':
        return df[applied_price].ewm(alpha=1/ma_period).mean().shift(ma_shift)
    elif ma_method == 'LWMA':
        y = pd.Series(0.0, index=df.index)
        for i in range(len(y)):
            if i<ma_period-1: y[i] = 'NaN'
            else:
                y[i] = 0
                for j in range(ma_period):
                    y[i] += df[applied_price][i-j]*(ma_period-j)
                y[i] /= ma_period*(ma_period+1)/2
        return y.shift(ma_shift)
    else: return df[applied_price].copy().shift(ma_shift)

r = pd.read_csv('quote.csv',sep=',', \
    #names=('DATE','USD','GBP','EUR','CAD','CHF','AUD','NZD','ZAR'), \
    dtype=[('DATE','S10'), \
            ('USD','f8'),\
            ('GBP','f8'),\
            ('EUR','f8'),\
            ('CAD','f8'),\
            ('CHF','f8'),\
            ('AUD','f8'),\
            ('NZD','f8'),\
            ('ZAR','f8'),\
            ('JPY','f8')], \
    parse_dates=True)

ccy1='USD'
ccy2='JPY'
rate = pd.DataFrame( {'DATE': [], 'CLOSE':[]} )
rate['CLOSE'] = r[ccy1]/r[ccy2]
rate['DATE'] = r['DATE']


#rate = pd.read_csv('zarjpy.csv',sep=',', names=('DATE','CLOSE'), parse_dates=True)

#macd = pd.DataFrame({'MACD':iMA(rate, 12) - iMA(rate, 26)})
#signal = pd.DataFrame({'SIGNAL':iMA(macd,9,applied_price='MACD')})

macd = iMA(rate, SHORT_TERM) - iMA(rate, LONG_TERM)
macddf = pd.DataFrame({'MACD':macd})

signal = iMA( macddf, SIGNAL_TERM, applied_price='MACD')

#signal = pd.DataFrame({'SIGNAL':iMA(macd,9,applied_price='MACD')})



macdset = pd.DataFrame({'MACD': macd, 'SIGNAL': signal})

def BuyEntry(i): # 買いシグナル
    if macdset['MACD'][i] <0.0 \
    and macdset['MACD'][i-1] < macdset['SIGNAL'][i-1] \
    and macdset['MACD'][i] > macdset['SIGNAL'][i]:
        return True
    
    #if macdset['MACD'][i] <0.0 and macdset['MACD'][i] > macdset['SIGNAL'][i]:
    #    return True
    #if macdset['MACD'][i] > macdset['SIGNAL'][i]:
    #    return True

def SellEntry(i): # 売りシグナル
    if macdset['MACD'][i] > 0.0 \
    and  macdset['MACD'][i-1] > macdset['SIGNAL'][i-1] \
    and macdset['MACD'][i] < macdset['SIGNAL'][i]:
        return True
    
    #if  macdset['MACD'][i] > 0.0 and  macdset['MACD'][i] < macdset['SIGNAL'][i]:
    #    return True
    #if  macdset['MACD'][i] < macdset['SIGNAL'][i]:
    #    return True
    

def BuyExit(i): # 買いポジション決済シグナル
    #if macdset['MACD'][i] - macdset['SIGNAL'][i] \
    #    < macdset['MACD'][i-1] - macdset['SIGNAL'][i-1]:
    #    return True
    #return SellEntry(i)
    if LongPos - rate['CLOSE'][i] >0.5:
        return True
    if macdset['MACD'][i] < 0.0:
        return True
    
def SellExit(i): # 売りポジション決済シグナル
    #if macdset['MACD'][i] - macdset['SIGNAL'][i] \
    #    > macdset['MACD'][i-1] - macdset['SIGNAL'][i-1]:
    #    return True
    #return BuyEntry(i)
    if macdset['MACD'][i] > 0.0:
        return True
    
    if ShortPos - rate['CLOSE'][i] <-0.5:
        return True

#LongPos = pd.Series(0.0, index=rate.index) # 買いポジション情報
#ShortPos = LongPos.copy() # 売りポジション情報
Lots = 1.0 # 売買ロット数

#LongPos = pd.DataFrame(colums['DATE','RATE','PROFIT'])
#ShortPos = pd.DataFrame()


LongPos = 0.0
ShortPos =0.0
TotalProfit =0.0
PROFIT = 0.0
for i in range( SHORT_TERM+LONG_TERM+SIGNAL_TERM+1, len(rate)-1 ):

    if BuyEntry(i) and LongPos == 0.0: 
       # LongPos[i+1] = Lots # 買いシグナル
       # LongPos.append( pd.DataFrame( {'DATE':rate['DATE'][i], 'RATE':rate['CLOSE']} ) )
        LongPos = rate['CLOSE'][i]
        if ShortPos!= 0.0:
            PROFIT = rate['CLOSE'][i] - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT
            print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
            
    elif SellEntry(i) and ShortPos == 0.0: 
        # LongPos[i+1] = Lots # 買いシグナル
        # LongPos.append( pd.DataFrame( {'DATE':rate['DATE'][i], 'RATE':rate['CLOSE']} ) )
        ShortPos = rate['CLOSE'][i]
        if LongPos!= 0.0:
            PROFIT =  rate['CLOSE'][i] - LongPos
            TotalProfit = TotalProfit + PROFIT
            print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0
    elif BuyExit(i) and LongPos != 0.0:
        PROFIT = rate['CLOSE'][i] - LongPos
        TotalProfit = TotalProfit + PROFIT
        print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], LongPos, PROFIT) )
        LongPos = 0.0
    elif SellExit(i) and ShortPos != 0.0: 
        PROFIT = rate['CLOSE'][i] - ShortPos
        PROFIT = -PROFIT
        TotalProfit = TotalProfit + PROFIT
        print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
        ShortPos = 0.0


print('total')
print(TotalProfit)
print(LongPos)
print(ShortPos)