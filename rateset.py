import numpy as np
import pandas as pd
import techlib as tl
import sys
import logging
import glob
import mysql.connector as mydb
import csv
import json
import copy
class RateSet:
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    path = "C:\\src\\fx03\\rate\\daily\\" # use your path
    all_files = glob.glob(path + "fx_result_*.csv")

    def __init__(self):
        self.conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx',
            auth_plugin='mysql_native_password'
        )

        # コネクションが切れた時に再接続してくれるよう設定
        self.conn.ping(reconnect=True)

        # 接続できているかどうか確認
        #print(self.conn.is_connected())
        self.cur = self.conn.cursor()

        self.macd_short_term = 12
        self.macd_long_term = 26
        self.macd_signal_term = 9
        self.open_threshold = 2.0
        self.close_threshold = 1.5
        self.corr_term = 7
        self.rci_short_term = 26
        self.rci_long_term = 26
        self.rci_threshold = 80

        self.rci_term01 = 9
        self.rci_term02 = 26
        self.rci_term03 = 13
        self.rci_term04 = 13

        self.avg_term01 = 10
        self.avg_term02 = 25
        self.avg_term03 = 75

        self.from_date = "20210101"
        self.to_date = "20220630"

        self.ema_mode = "STRICT"




    def _getrate(self, ccy):
        cur.execute("SELECT * FROM rate1m where CCY = %s ORDER BY DATE,TIME", (ccy,))
        #cur.execute("SELECT * FROM rate1m where CCY = %s  and DATE like '201910%'ORDER BY DATE,TIME", (ccy,))

        #5minute
        #cur.execute("SELECT * FROM rate1m where CCY = %s  and (TIME like '%000' or TIME like '%500') ORDER BY DATE,TIME", (ccy,))

        #10minute
        #cur.execute("SELECT * FROM rate1m where CCY = %s  and (TIME like '%000') ORDER BY DATE,TIME", (ccy,))
        #15minute
        #cur.execute("SELECT * FROM rate1m where CCY = %s  and (TIME like '%0000' or TIME like '%1500'or TIME like '%3000' or TIME like '%4500') ORDER BY DATE,TIME", (ccy,))
        #1hour
        #cur.execute("SELECT * FROM rate1m where CCY = %s  and (TIME like '%0000') ORDER BY DATE,TIME", (ccy,))
        rows = cur.fetchall()


        line =[]
        r =[]

        high = 0
        row = 99999
        if Term > 1:
            for i in range(len(rows)):

                if i % Term == 0:
                    high = rows[i][3]
                    row = rows[i][3]
                    line = [rows[i][0], rows[i][1], rows[i][2], rows[i][3]]


                if rows[i][4] > high:
                    high = rows[i][4]

                if rows[i][5] < row:
                    row = rows[i][5]

                if i % Term == Term-1:
                    line.extend([high, row, rows[i][6]])
                    r.append(line)
                    #print(r)

            rows = r

        rate = pd.DataFrame(rows,
                    columns=["CCY","DATE", "TIME", "OPEN","HIGH","LOW","CLOSE"])

        #rate = DataFrame(rows)
        #rate.columns[["CCY","DATE", "TIME", "OPEN","HIGH","LOW","CLOSE"]]

        return rate

    def  set_macd_term(self,macd_short_term, macd_long_term, macd_signal_term):
        self.macd_short_term = macd_short_term
        self.macd_long_term = macd_long_term
        self.macd_signal_term = macd_signal_term

    def set_avg_term(self, term):
        self.avg_term = term

    def getrate15m(self,ccy, step):
        sql = "SELECT * FROM rate1m where date >= %s and date <= %s and CCY = %s and ( TIME like '%0000' or TIME like '%1500' or TIME like '%3000' or TIME like '%4500') ORDER BY DATE,TIME"
        return self.getrate_common(ccy,step, sql)

    def getrate5m(self,ccy, step):
        sql = "SELECT * FROM rate1m where date >= %s and date <= %s and CCY = %s and ( TIME like '%000' or TIME like '%500') ORDER BY DATE,TIME"
        return self.getrate_common(ccy,step, sql)

    def getrate10m(self,ccy, step):
        sql = "SELECT * FROM rate1m where date >= %s and date <= %s and time like '%000' and CCY = %s ORDER BY DATE,TIME"
        return self.getrate_common(ccy,step, sql)

    def getrate1h(self,ccy, step):
        sql = "SELECT * FROM rate1m where date >= %s and date <= %s and time like '%0000' and CCY = %s ORDER BY DATE,TIME"
        return self.getrate_common(ccy,step, sql)

    def getrate1h_desc(self, ccy, to_date, to_time, limit):
        sql = "SELECT * FROM rate1m where date <= %s and time like '%0000' and CCY = %s ORDER BY DATE desc,TIME desc Limit %s"
        self.cur.execute(sql, (to_date, ccy, limit, ))
        rows =  self.cur.fetchall()
        r = []
        for row in rows:
            if row[1] + row[2] > to_date + to_time:
                continue

            r.append( list(row) )

        #print(r)
        return r

    def getrate4h_desc(self, ccy, to_date, to_time, limit):
        sql = "SELECT * FROM rate1m where date <= %s and time like '%0000' and CCY = %s and TIME in ('230000','030000','070000','110000','150000','190000') ORDER BY DATE desc,TIME desc Limit %s"
        self.cur.execute(sql, (to_date, ccy, limit, ))
        rows =  self.cur.fetchall()
        r = []
        for row in rows:
            if row[1] + row[2] > to_date + to_time:
                continue

            r.append( list(row) )

        #print(r)
        return r


    def getrate1d(self,ccy, step):
        #sql = "SELECT * FROM rate1m where date >= %s and date <= %s  and time like '000000' and CCY = %s ORDER BY DATE,TIME"
        sql = "SELECT * FROM rate1m where date >= %s and date <= %s  and time like '000000' and CCY = %s ORDER BY DATE,TIME"
        return self.getrate_common(ccy,step, sql)

    def getrate8h(self,ccy, step):
        sql = "SELECT * FROM rate1m where date >= %s and date <= %s and CCY = %s and TIME in ('000000','080000','160000') ORDER BY DATE,TIME"
        return self.getrate_common(ccy,step, sql)

    def getrate4h(self,ccy, step):
        sql = "SELECT * FROM rate1m where date >= %s and date <= %s  and CCY = %s and TIME in ('230000','030000','070000','110000','150000','190000') ORDER BY DATE,TIME"
        return self.getrate_common(ccy,step, sql)


    def getrate_common(self,ccy, step, sql):
        #self.cur.execute("SELECT * FROM rate1d where date > '20180101' and CCY = %s ORDER BY DATE,TIME", (ccy,))
        self.cur.execute(sql, (self.from_date, self.to_date, ccy,))
        rows =  self.cur.fetchall()
        r = []
        for row in rows:
            r.append( list(row) )

        return r

    def init_dataframe(self, rate_list, step):

        #r.pop()

        self.ema(rate_list, self.macd_short_term, step )
        self.ema(rate_list, self.macd_long_term, step )
        self.macd(rate_list, self.macd_signal_term, step)

        self.corr(rate_list, self.corr_term)
        self.rci(rate_list, self.rci_short_term)
        self.rci(rate_list, self.rci_long_term)
        self.rci(rate_list, self.rci_term01)
        self.rci(rate_list, self.rci_term02)
        self.rci(rate_list, self.rci_term03)
        self.rci(rate_list, self.rci_term04)

        del rate_list[:step*(self.macd_long_term + self.macd_signal_term)+1]

        #f = open('list.txt', 'w')
        #for x in r:
        #    f.write(str(x) + "\n")
        #f.close()

        #print(r)
        rate = pd.DataFrame(rate_list,
                    columns=["CCY","DATE", "TIME", "OPEN","HIGH","LOW","CLOSE","SHORT_SMA","LONG_SMA","MACD","SIGNAL","CORR","SHORT_RCI","LONG_RCI","RCI_TERM01","RCI_TERM02","RCI_TERM03","RCI_TERM04"])
        rate['RCI_THRESH'] = self.rci_threshold
        rate['DIFF'] = ( rate['MACD'] - rate['SIGNAL'] )/abs( rate['MACD'])
        rate['AVG'] = rate['CLOSE'].rolling(self.macd_short_term).mean()
        rate['STDEV'] = rate['CLOSE'].rolling(self.macd_short_term).std()

        rate['AVG01'] = rate['CLOSE'].rolling(self.avg_term01).mean()
        rate['AVG02'] = rate['CLOSE'].rolling(self.avg_term02).mean()
        rate['AVG03'] = rate['CLOSE'].rolling(self.avg_term03).mean()
        rate['STDEV01'] = rate['CLOSE'].rolling(self.avg_term01).std()
        rate['STDEV02'] = rate['CLOSE'].rolling(self.avg_term02).std()
        rate['STDEV03'] = rate['CLOSE'].rolling(self.avg_term03).std()

        rate['EMA01'] = rate['CLOSE'].ewm(span=self.avg_term01, adjust=False).mean()
        rate['EMA02'] = rate['CLOSE'].ewm(span=self.avg_term02, adjust=False).mean()
        rate['EMA03'] = rate['CLOSE'].ewm(span=self.avg_term03, adjust=False).mean()

        #print(rate.dtypes)
        return rate

    def getrate(self,ccy):
        self.cur.execute("SELECT * FROM rate1m where CCY = %s ORDER BY DATE,TIME", (ccy,))
        rows = self.cur.fetchall()

        line =[]
        r =[]

        high = 0
        low = 99999
        for i in range(len(rows)):

            if  high == 0:
                high = rows[i][3]
                low = rows[i][3]
                line = [rows[i][0], rows[i][1], rows[i][2], rows[i][3]] #Openまでリストにセット


            if rows[i][4] > high:
                high = rows[i][4]

            if rows[i][5] < low:
                low = rows[i][5]

            if rows[i][2]== "225900":
                line.extend([high, low, rows[i][6]]) #High以降をリストにセット
                r.append(line)
                high = 0
                low = 999999

                #print(r)
        #r.pop(0)


        self.ema(r, self.macd_short_term, 5 )

        self.ema(r, self.macd_long_term, 5 )
        self.macd(r, self.macd_signal_term, 5)
        del r[:5*(self.macd_long_term + self.macd_signal_term)+1]
        f = open('list.txt', 'w')
        for x in r:
            f.write(str(x) + "\n")
        f.close()

        #print(r)
        rate = pd.DataFrame(r,
                    columns=["CCY","DATE", "TIME", "OPEN","HIGH","LOW","CLOSE","SHORT_SMA","LONG_SMA","MACD","SIGNAL"])

        rate['AVG'] = rate['CLOSE'].rolling(21).mean()
        rate['STDEV'] = rate['CLOSE'].rolling(21).std()

        #print(rate.dtypes)
        return rate



    def initrate(self,rate):
        #macd = tl.iMA(rate, SHORT_TERM) - tl.iMA(rate, LONG_TERM)
        #macddf = pd.DataFrame({'MACD':macd})
        #signal = tl.iMA( macddf, SIGNAL_TERM, applied_price='MACD')
        #rate["MACD"] = macd
        #rate["SIGNAL"] = signal
        rate['STDEV'] = tl.stdev(rate, 21)
        rate['AVG01'] = rate['CLOSE'].rolling(self.avg_term01).mean()
        rate['AVG02'] = rate['CLOSE'].rolling(self.avg_term02).mean()
        rate['AVG03'] = rate['CLOSE'].rolling(self.avg_term03).mean()


        macd_array = rate["MACD"].values.tolist()
        avg_array = rate["AVG21"].values.tolist()

        #print(avg_array)
        corr_array = []
        for i in range(len(rate)):
            if i<21:
                corr_array.append(0.0)
                continue

            v = np.corrcoef(macd_array[i-21:i], avg_array[i-21:i])
            corr_array.append(v[0,1])

        #print(corr_array)
        #n=11
        #rate["CORR"] = np.convolve(corr_array, np.ones(n)/float(n), 'same')
        #rate["CORR"] = corr_array
        return rate

    def make1day(self):
        f = open('CCYLIST.txt', 'r')
        ccylist = f.readlines()
        f.close()
        for i in range(len(ccylist)):
            self._make1day(ccylist[i][0:6])


    def _make1day(self, ccy):
        self.cur.execute("SELECT * FROM rate1m where CCY = %s ORDER BY DATE,TIME", (ccy,))
        rows = self.cur.fetchall()

        line = []
        high = 0
        low = 99999
        last_time = "230000"
        for i in range(len(rows)):
            current_time = rows[i][2] #TIME
            if current_time >= "230000" and last_time <"230000":
                line.append(high)
                line.append(low)
                line.append(rows[i-1][6])

                print(line)
                #<TICKER>,<DTYYYYMMDD>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>
                self.cur.execute("INSERT INTO rate1d VALUES (%s, %s, %s, %s, %s, %s, %s)", line)
                self.conn.commit()
                #print(line)
                high = 0
                low = 999999
                ling = []

            if  high == 0:
                high = rows[i][3]
                low = rows[i][3]
                line = [rows[i][0], rows[i][1], rows[i][2], rows[i][3]] #Openまでリストにセット


            if rows[i][4] > high:
                high = rows[i][4]

            if rows[i][5] < low:
                low = rows[i][5]

            last_time = current_time



    '''
    def getrate():
        li = []

        for filename in all_files:
            logger.debug(filename)
            d = pd.read_csv(filename, header=None,delimiter=",")
            li.append(d)

        df = pd.concat(li, axis=0, ignore_index=True)
        df.columns=["CCY","CCY_DETAIL","DATE","SETTLE_PRE","OPEN","HIGH","LOW","CLOSE","SETTLE","RATIO","SWAP","TRAN_VOL","SETTLE_VOL"]

        return df

    def initrate(df, ccy1, ccy2):
        rate = df[["CCY","DATE","OPEN","HIGH","LOW","CLOSE"]]
        rate = rate[rate["CCY"] == ccy1+"/"+ccy2]
        rate = rate.sort_values("DATE")


        macd = tl.iMA(rate, SHORT_TERM) - tl.iMA(rate, LONG_TERM)
        macddf = pd.DataFrame({'MACD':macd})
        signal = tl.iMA( macddf, SIGNAL_TERM, applied_price='MACD')

        rate["MACD"] = macd
        rate["SIGNAL"] = signal

        rate['STDEV'] = tl.stdev(rate, 21)


        rate['AVG21'] = rate['CLOSE'].rolling(21).mean()

        rate = rate.reset_index()

        return rate
    '''

    def ema(self, r, term, skip_term):
        alpha = 2.0/(term+1)
        '''
        for i in range(term*skip_term, len(r)):
            if i==term*skip_term:
                sum = 0
                for j in range(i, i-term*skip_term, -skip_term):
                    sum = sum + r[j][6]
                #avg = (sum + 2*r[i][6]+ r[i-1][6]) /( term + 1 + 2 )
                #avg = (sum + r[i][6]) /( term + 1 )
                avg = sum /term
                avg_before = avg
            else:
                sum = 0
                for j in range(i, i-term*skip_term, -skip_term):
                    sum = sum + r[j][6]

                #avg = (sum + r[i][6]) /( term + 1 )
                avg = avg_before + alpha * (r[i][6] - avg_before )
                avg_before = avg

            r[i].append(avg)


        '''
        for i in range(term*skip_term, len(r)):
            if i==term*skip_term:
                sum = 0
                for j in range(i, i-term*skip_term, -skip_term):
                    sum = sum + r[j][6]

                if self.ema_mode == "STRICT":
                    avg = (sum + 2*r[i][6]+ r[i-1][6]) /( term + 1 + 2 )
                else:
                    avg = (sum + r[i][6]) /( term + 1 )

                #avg = sum /term
                avg_before = avg
            else:
                sum = 0
                for j in range(i, i-term*skip_term, -skip_term):
                    sum = sum + r[j][6]

                if self.ema_mode == "STRICT":
                    avg = (sum + 2*r[i][6]+ r[i-1][6]) /( term + 1 + 2 )
                else:
                    avg = (sum + r[i][6]) /( term + 1 )

                #avg = avg_before + alpha * (r[i][6] - avg_before )
                avg_before = avg

            r[i].append(avg)


    def macd(self, r, signal_term, skip_term):
        for i in range(0, len(r)):
            try:
                md = r[i][7] - r[i][8] #ShortEMA-LongEMA
                r[i].append(md)
            except IndexError:
                continue

        for i in range(0, len(r)):
            try:
                sum = 0
                for j in range(i, i-signal_term*skip_term, -skip_term):
                    sum = sum + r[j][9]
                avg = sum /signal_term
                r[i].append(avg)
            except IndexError:
                continue


    def corr(self, r, term):
        for i in range(term, len(r)):
            sum = 0
            for j in range(i-term+1, i+1):
                sum = sum + r[j][6]
            avg = sum  /term

            stdev1 = 0
            stdev2 = 0
            conv = 0
            dev1 = 0
            dev2 = 0
            for j in range(i-term+1, i+1):
                dev1 = ( r[j][6] - avg )
                dev2 = ( j - (2*i+2-1 -term)/2 )

                #print(i,j,(i+1)/2,dev2)

                conv = conv + dev1*dev2
                stdev1 = stdev1 + dev1**2
                stdev2 = stdev2 + dev2**2
            #print(i,stdev2)

            stdev1 = ( stdev1/term )**0.5
            stdev2 = ( stdev2/term )**0.5
            conv = conv/term
            if stdev1 == 0:
                stdev1 = 100000
            if stdev2 == 0:
                stdev2 = 100000

            r[i].append( conv/(stdev1*stdev2 ))

    def rci(self, r, term):
        for i in range(term, len(r)):
            sum = 0
            time_order = term
            for j in range(i-term+1, i+1):
                order = 1
                for k in range(i-term+1, i+1):
                    if r[j][6] < r[k][6]:
                        order = order + 1
                    else:
                        continue
                sum = sum + ( time_order - order )**2
                time_order = time_order - 1


            r[i].append( (1-(6*sum)/(term**3-term)) * 100 )


    def getrate1d_all(self, short_term, long_term, signal_term):
        ccylist = []
        f = open('CCYLIST.txt', 'r')
        ccylist = f.readlines()
        f.close()

        for j in range(len(ccylist)):
            ccy1 = ccylist[j][0:3]
            ccy2 = ccylist[j][3:6]
            self.set_macd_term(short_term, long_term, signal_term)
            rate = self.getrate1d(ccy1+ccy2)

            if ccy2 not in ["JPY"]:
                rate["DIFF"] = ( rate["MACD"]-rate["SIGNAL"]  )*100
            else:
                rate["DIFF"] = ( rate["MACD"]-rate["SIGNAL"]  )

            #print(rate[-10:].to_json() )
            print(rate[-10:] )



if __name__ == '__main__':
    #r = getrate()
    #rate = initrate(r,"USD", "JPY")

    rs = RateSet()

    rs.to_date = "20220930"


    ccylist = []
    f = open('CCYLIST.txt', 'r')
    ccylist = f.readlines()
    f.close()

    result_lis = []
    for c in copy.copy(ccylist):
        if c[0:1] == "#":
            ccylist.remove(c)

    term1 = 10
    term2 = 20
    term3 = 10
    term4 = 20

    for ccy in ccylist:
        print(ccy)
        rs.rci_term01 = term1
        rs.rci_term02 = term2
        rs.rci_term03 = term3
        rs.rci_term04 = term4
        rate_list = rs.getrate1d(ccy[0:6],1)
        #rate = rs.getrate10m(ccy1+ccy2,1)
        #rate = rs.getrate8h(ccy1+ccy2,1)
        #rate = rs.getrate4h(ccy1+ccy2,1)

        rate = rs.init_dataframe(rate_list,1)

        print(rate)

        '''
        rs.cur.execute(sql, (ccy[0:6],))
        rows =  rs.cur.fetchall()
        print("ccy:" + ccy[0:6])

        ccy_dic = {}
        ccy_dic["ccy"] = ccy[0:6]
        rate_lis = []
        for row in rows:
            rate_dic={}
            rate_dic["date"] = row[1]
            rate_dic["time"] = row[2]
            rate_dic["open"] = row[3]
            rate_dic["high"] = row[4]
            rate_dic["low"] = row[5]
            rate_dic["close"] = row[6]
            rate_lis.append(rate_dic)

        ccy_dic["list"] = rate_lis
        result_lis.append(ccy_dic)
        '''
    #print(result_lis)

    #jf =open("rate1h.json","w")
    #json.dump(result_lis, jf, indent=2)
    #jf.close()
