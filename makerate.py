import numpy as np
import pandas as pd
import techlib as tl


rate = pd.read_csv('quote.csv',sep=',', \
    #names=('DATE','USD','GBP','EUR','CAD','CHF','AUD','NZD','ZAR'), \
    dtype=[('DATE','S10'), \
            ('USD','f8'),\
            ('GBP','f8'),\
            ('EUR','f8'),\
            ('CAD','f8'),\
            ('CHF','f8'),\
            ('AUD','f8'),\
            ('NZD','f8'),\
            ('ZAR','f8'),\
            ('JPY','f8')], \
    parse_dates=True)

ccy1='GBP'
ccy2='JPY'

r = pd.DataFrame( {'DATE': [], 'CLOSE':[]} )
r['DATE'] = rate['DATE']
r['CLOSE']=rate[ccy1]/rate[ccy2]

print(tl.iMA(r, 9, applied_price='CLOSE', ma_method='EMA'))

#r = r.rename(columns={'': 'a'}, index={'ONE': 'one'})

def stdev(df, ma_period, ma_shift=0,  applied_price='CLOSE'):
    return df[applied_price].rolling(ma_period).std()

'''
std = stdev(r, 9)
d=[]
diff = pd.DataFrame( {'DIFF':[]} )
print(std)
for i in range( 21, len(rate)-1):
    d = d+[std[i]-std[i-1]]


diff['DIFF'] = d

print(diff)

'''