import numpy as np
import pandas as pd
import techlib as tl
import rateset
import result
import sys
import logging
import copy

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


LossCutPips = 5
ProfitPips = 45
PipsRatio = 1.0

Spread = 0.5
BuyFlag = False
SellFlag = False

BuyRate = 0.0
SellRate = 0.0
JudgeCnt = 0

RCIThreshold = 0.9


def BuyEntry(i, rate): # 買いシグナル
    if rate['SHORT_RCI'][i-1] < rate['LONG_RCI'][i-1]  and rate['SHORT_RCI'][i] > rate['LONG_RCI'][i]:
        return True

    return False

def SellEntry(i, rate): # 売りシグナル
    if rate['SHORT_RCI'][i-1] > rate['LONG_RCI'][i-1]  and rate['SHORT_RCI'][i] < rate['LONG_RCI'][i]:  
        return True
    
    return False

def BuyExit(i, p, rate): # 買いポジション決済シグナル
    if rate['CLOSE'][i] - p < -LossCutPips*0.01:
        return True,  rate['CLOSE'][i]
    if rate['CLOSE'][i] - p - Spread*0.01 > ProfitPips*0.01:
        return True,  rate['CLOSE'][i]

    if rate['LONG_RCI'][i] > RCIThreshold:
        return True, rate['CLOSE'][i]

 
    if SellEntry(i, rate):
        return True, rate['CLOSE'][i]
    
    return False, 0.0

def SellExit(i, p, rate): # 売りポジション決済シグナル

    if rate['CLOSE'][i] - p > LossCutPips*0.01:
        return True, rate['CLOSE'][i]
    if rate['CLOSE'][i] - p + Spread*0.01 < -ProfitPips*0.01:
        return True, rate['CLOSE'][i]

    if rate['LONG_RCI'][i] < -RCIThreshold:
        return True,  rate['CLOSE'][i]


    if BuyEntry(i, rate):
        return True, rate['CLOSE'][i]
    
    return False, 0.0

Lots = 1.0 # 売買ロット数


def sim(rate):
    TotalProfit =0.0
    PROFIT = 0.0
    NumWin = 0
    NumLoss = 0
    LongPos = 0.0
    ShortPos =0.0
    LongNum = 0
    ShortNum = 0
    MaxConsvWin =0
    ConsvWin = 0
    MaxConsvLoss = 0
    ConsvLoss = 0
    BuyFlag = False
    SellFlag = False


    for i in range(1, len(rate)-1 ):
        if LongPos != 0.0 and BuyExit(i, LongPos, rate)[0]:
            #PROFIT = rate['CLOSE'][i] - LongPos
            PROFIT = BuyExit(i, LongPos, rate)[1] - LongPos
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE BUY {} {:.2f} {:.2f}  {:.4f}".format(rate['DATE'][i], rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0
            LongNum = 0
            if PROFIT > 0.0:
                NumWin = NumWin + 1
                ConsvWin += 1
                if ConsvWin > MaxConsvWin:
                    MaxConsvWin = ConsvWin
                ConsvLoss = 0

            else:
                NumLoss = NumLoss + 1
                ConsvWin = 0
                ConsvLoss += 1
                if ConsvLoss > MaxConsvLoss:
                    MaxConsvLoss = ConsvLoss

        if ShortPos != 0.0  and  SellExit(i, ShortPos, rate)[0]: 
            PROFIT = SellExit(i, ShortPos, rate)[1]  - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE SELL {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
            ShortNum = 0

            if PROFIT > 0.0:
                NumWin = NumWin + 1
                ConsvWin += 1
                if ConsvWin > MaxConsvWin:
                    MaxConsvWin = ConsvWin
                ConsvLoss = 0
            else:
                NumLoss = NumLoss + 1
                ConsvWin = 0
                ConsvLoss += 1
                if ConsvLoss > MaxConsvLoss:
                    MaxConsvLoss = ConsvLoss
                
                            
        if SellEntry(i, rate) and ShortNum == 0 : 
            ShortPos = rate['CLOSE'][i]
            ShortNum += 1

        if BuyEntry(i,rate) and LongNum==0: 
            LongPos = rate['CLOSE'][i]
            LongNum += 1
            BuyFlag = False            

    r  = result.Result()
    r.num_win = NumWin
    r.num_loss = NumLoss
    r.max_consv_win = MaxConsvWin
    r.max_consv_loss = MaxConsvLoss
    r.total_profit = TotalProfit

    #logger.info(rate['CLOSE'][i])
    logger.info(NumWin)
    logger.info(NumLoss)
    if NumWin != 0 and NumLoss != 0:
        logger.info(NumWin/(NumWin+NumLoss) )
    
    logger.info( MaxConsvWin)
    logger.info( MaxConsvLoss)
    logger.info(LongPos)
    logger.info(ShortPos)

    return r

if __name__ == '__main__':

    #r = rateset.getrate()
    #rate = rateset.initrate(r,"USD", "JPY")
    
    if len(sys.argv) > 5:
        ccy1=sys.argv[1]
        ccy2=sys.argv[2]
        ProfitPips = int(sys.argv[3])
        LossCutPips = int(sys.argv[4])
        Spread = float( sys.argv[5])
        term1 = int(sys.argv[6])
        term2 = int(sys.argv[7])
        RCIThreshold = float( sys.argv[8])


        if ccy2!='JPY':
            LossCutPips = LossCutPips/100
            ProfitPips = ProfitPips/100
            PipsRatio = PipsRatio/100
            Spread = Spread /100

        rs = rateset.RateSet()
        rs.rci_short_term = term1
        rs.rci_long_term = term2

        rate = rs.getrate1d(ccy1+ccy2,1)


        r = sim(rate)
        log =ccy1+" "+ccy2+" "+"{:.4f}".format(r.total_profit) 

        #log =ccy1+" "+ccy2+" "+"{:.4f}".format(total) 
        logger.info(log)

    else:    
        start_idx = int(sys.argv[1])
        end_idx = int(sys.argv[2])
        skip_term = int(sys.argv[3])
        result_file = sys.argv[4]

        #CCY = ["JPY", "USD", "GBP", "EUR", "AUD", "NZD", "CAD", "CHF", "ZAR"]
        ccylist = []
        f = open('CCYLIST.txt', 'r')
        ccylist = f.readlines()
        f.close()

        for c in copy.copy(ccylist):
            if c[0:1] == "#":
                ccylist.remove(c)


        param_list = []
        prev_ccy="" 

        for i in range(start_idx, end_idx, skip_term):           #term1
            for j in range(start_idx+5, end_idx, skip_term):      #term2
                for k in range(70, 90, 5):      #rci
                    for p in range(100,400,50):              #profit
                        for l in range(100,400,50):          #losscut
                            param_list.append([i,j,k,p,l])

        for j in range(len(ccylist)):
            for i in range(len(param_list)):
                
                term1 = param_list[i][0]
                term2 = param_list[i][1]

                RCIThreshold = param_list[i][2]
                ProfitPips = param_list[i][3]
                LossCutPips = param_list[i][4]

                ccy1 = ccylist[j][0:3]
                ccy2 = ccylist[j][3:6]
                print(ccy1 + "/" + ccy2 )

                #if ccy2=='GBP' or ccy1=='GBP':
                #    LossCutPips = LossCutPips*1.5
                #    ProfitPips = ProfitPips*1.5


                Spread = 0.5
                if ccy2 not in ["JPY", "HKD", "MXN", "TRY", "ZAR"]:
                    LossCutPips = LossCutPips/100
                    ProfitPips = ProfitPips/100
                    PipsRatio = PipsRatio/100
                    Spread = Spread /100
                

                rs = rateset.RateSet()
                if rs.rci_short_term != term1 and rs.rci_long_term != term2 and prev_ccy != ccy1+ccy2:

                    rs.rci_short_term = term1
                    rs.rci_long_term = term2
                
                    #rate = rs.getrate1d(ccy1+ccy2,1)
                    rate = rs.getrate1h(ccy1+ccy2,1)
                    #rate = rs.getrate10m(ccy1+ccy2,1)
                    #rate = rs.getrate8h(ccy1+ccy2,1)
                    #rate = rs.getrate4h(ccy1+ccy2,1)

                    prev_ccy = ccy1+ccy2

                

                r = sim(rate)
                r.ccy = ccy1+ccy2
                
                r.macd_short_term = param_list[i][0]
                r.macd_long_term = param_list[i][1]
                r.macd_signal_term = param_list[i][2]

                r.target_profit = param_list[i][3]
                r.target_losscut = param_list[i][4]
                
                if ccy2 not in ["JPY", "HKD", "MXN", "TRY","ZAR"]:
                #if ccy2!='JPY':
                    r.total_profit = r.total_profit * 100

                log =ccy1+" "+ccy2+" "+"{:.4f}".format(r.total_profit) 
                
                    
                r.save(result_file)
                logger.info(log)
        """

        for i in range(9):
            for j in range(9):
                LossCutPips = 30
                ccy1 = CCY[j]
                ccy2 = CCY[i]
                if ccy1 == ccy2:
                    continue
                if ccy1 == "GBP" or ccy2 == "GBP":
                    LossCutPips = 30
                if ccy2!='JPY':
                    LossCutPips = LossCutPips/100
                r, m, s = initrate(ccy1, ccy2)
                sim(r,m)
    """