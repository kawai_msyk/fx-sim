import urllib.request
import mysql.connector as mydb
import glob
import csv
import os
 
class ResultLoader:


    def insert_db(self):
        conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx'
        )

        # コネクションが切れた時に再接続してくれるよう設定
        conn.ping(reconnect=True)

        # 接続できているかどうか確認
        print(conn.is_connected())
        cur = conn.cursor()
        records = []
        with open("./result.csv") as f:
            for r in csv.reader(f):
                records.append(r)
            print(records)
            cur.executemany("INSERT INTO result_macd VALUES (1,%s, %s, %s, %s, %s, %s, %s, %s)", records)
            conn.commit()

if __name__ == '__main__':
    r =  ResultLoader()
    r.insert_db()
    #print(r.get_ccy("01"))

    