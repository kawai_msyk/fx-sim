import numpy as np
import pandas as pd
import techlib as tl
import rateset
import sys
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


LossCutPips = 5
ProfitPips = 45
PipsRatio = 1.0

Spread = 0.5
BuyFlag = False
BuyRate = 0.0

SellFlag = False
SellRate = 0.0




def BuyEntry(i, rate): # 買いシグナル
    #if rate['MACD'][i] < 0.0 and rate['MACD'][i-1] < rate['SIGNAL'][i-1]  and rate['MACD'][i] > rate['SIGNAL'][i]:
    if rate['MACD'][i-1] < rate['SIGNAL'][i-1]  and rate['MACD'][i] > rate['SIGNAL'][i]:
    #if rate['MACD'][i] - rate['MACD'][i-1] > rate['MACD'][i-1] - rate['MACD'][i-2]:
        global ProfitPips
        #ProfitPips = min(rate['STDEV'][i]*1000, 30)
        #ProfitPips = max(min(rate['STDEV'][i]*1000, 50), 10)
        #ProfitPips = ProfitPips * PipsRatio

        return True

    return False

def SellEntry(i, rate): # 売りシグナル
    #if rate['MACD'][i] > 0.0 and rate['MACD'][i-1] > rate['SIGNAL'][i-1]  and rate['MACD'][i] < rate['SIGNAL'][i]:
    if rate['MACD'][i-1] > rate['SIGNAL'][i-1]  and rate['MACD'][i] < rate['SIGNAL'][i]:
    #if rate['MACD'][i] - rate['MACD'][i-1] < rate['MACD'][i-1] - rate['MACD'][i-2]:
        #global ProfitPips
        #ProfitPips = min(rate['STDEV'][i]*1000, 30)
        #ProfitPips = max(min(rate['STDEV'][i]*1000, 50), 10)
        #ProfitPips = ProfitPips * PipsRatio
        return True

    return False

def BuyExit(i, p, rate): # 買いポジション決済シグナル
    #if rate['CLOSE'][i] - p < -LossCutPips*0.01:
    #    return True, p-LossCutPips*0.01

    if rate['LOW'][i] - p < -LossCutPips*0.01:
        return True, p - LossCutPips*0.01

    if rate['HIGH'][i] - p - Spread*0.01 > ProfitPips*0.01:
        return True, p + ProfitPips*0.01

    #if rate['HIGH'][i] - p - Spread*0.01 > LossCutPips*0.01 and ShortPos!=0:
    #    return True, p + LossCutPips*0.01


    #print(ProfitPips)
    #if SellEntry(i, rate):
    #    if p < rate['CLOSE'][i]:
    #        return True, rate['CLOSE'][i]

    return False, 0.0

def SellExit(i, p, rate): # 売りポジション決済シグナル
    #if rate['CLOSE'][i] - p > LossCutPips*0.01:
    #    return True, p + LossCutPips*0.01


    if rate['HIGH'][i] - p > LossCutPips*0.01:
        return True, p + LossCutPips*0.01

    #
    if rate['LOW'][i] - p + Spread*0.01 < -ProfitPips*0.01:
        return True, p - ProfitPips*0.01

    #if rate['LOW'][i] - p + Spread*0.01 < -LossCutPips*0.01 and LongPos!=0:
    #    return True, p - LossCutPips*0.01


    #if BuyEntry(i, rate):
    #    if p > rate['CLOSE'][i]:
    #        return True, rate['CLOSE'][i]

    return False, 0.0

Lots = 1.0 # 売買ロット数


def sim(rate):
    TotalProfit =0.0
    PROFIT = 0.0
    NumWin = 0
    NumLoss = 0
    LongPos = 0.0
    ShortPos =0.0
    LongNum = 0
    ShortNum = 0

    for i in range( 40, len(rate)-1 ):
        if LongPos != 0.0 and BuyExit(i, LongPos, rate)[0]:
            #PROFIT = rate['CLOSE'][i] - LongPos
            PROFIT = BuyExit(i, LongPos, rate)[1] - LongPos
            TotalProfit = TotalProfit + PROFIT*LongNum
            logger.debug("CLOSE BUY {} {:.2f} {:.2f}  {:.4f}".format(rate['DATE'][i], rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0
            LongNum = 0
            if PROFIT > 0.0:
                NumWin = NumWin + 1
            else:
                NumLoss = NumLoss + 1

        if ShortPos != 0.0  and  SellExit(i, ShortPos, rate)[0]: 
            PROFIT = SellExit(i, ShortPos, rate)[1]  - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT*ShortNum
            logger.debug("CLOSE SELL {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
            ShortNum = 0

            if PROFIT > 0.0:
                NumWin = NumWin + 1
            else:
                NumLoss = NumLoss + 1
                
        
        if BuyEntry(i,rate) and LongNum<5: 
            LongNum += 1
            LongPos = LongPos*(LongNum-1) + rate['CLOSE'][i]
            LongPos /=LongNum
            logger.debug("OPEN BUY {} {:.2f} {:.2f}  {:.2f}".format(rate['DATE'][i], rate['CLOSE'][i], LongPos, LongNum) )

                
        if SellEntry(i, rate) and ShortNum < 5 : 
            ShortNum += 1
            LongPos = ShortPos*(ShortNum-1) + rate['CLOSE'][i]
            ShortPos /= ShortNum
            logger.debug("OPEN SELL{} {:.2f} {:.2f}  {:.2f}".format(rate['DATE'][i], rate['CLOSE'][i], ShortPos, ShortNum) )


    logger.info(rate['CLOSE'][i])
    logger.info(NumWin)
    logger.info(NumLoss)
    logger.info(LongPos)
    logger.info(ShortPos)

    return TotalProfit

if __name__ == '__main__':

    #r = rateset.getrate()
    #rate = rateset.initrate(r,"USD", "JPY")
    
    if len(sys.argv) > 1:
        ccy1=sys.argv[1]
        ccy2=sys.argv[2]
        ProfitPips = int(sys.argv[3])
        LossCutPips = int(sys.argv[4])
        Spread = float( sys.argv[5])
        if ccy2!='JPY':
            LossCutPips = LossCutPips/100
            ProfitPips = ProfitPips/100
            PipsRatio = PipsRatio/100
            Spread = Spread /100
        r = rateset.getrate(ccy1+ccy2)
        rate = rateset.initrate(r)
        #rate = rateset.initrate(r,ccy1, ccy2)

        total = sim(rate)
        log =ccy1+" "+ccy2+" "+"{:.4f}".format(total) 
        logger.info(log)

    '''
    else:    
        CCY = ["JPY", "USD", "GBP", "EUR", "AUD", "NZD", "CAD", "CHF", "ZAR"]

        for i in range(9):
            for j in range(9):
                LossCutPips = 30
                ccy1 = CCY[j]
                ccy2 = CCY[i]
                if ccy1 == ccy2:
                    continue
                if ccy1 == "GBP" or ccy2 == "GBP":
                    LossCutPips = 30
                if ccy2!='JPY':
                    LossCutPips = LossCutPips/100
                r, m, s = initrate(ccy1, ccy2)
                sim(r,m)

    '''