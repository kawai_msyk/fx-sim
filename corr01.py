import numpy as np
import pandas as pd
import techlib as tl
import rateset
import result
import sys
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


LossCutPips = 5
ProfitPips = 45
PipsRatio = 1.0

Spread = 0.5
BuyFlag = False
SellFlag = False

BuyRate = 0.0
SellRate = 0.0
JudgeCnt = 0

CorrThreshold = 0.9


def BuyEntry(i, rate): # 買いシグナル
    if rate['CORR'][i-1] < 0  and rate['CORR'][i] > 0:
        return True

    return False

def SellEntry(i, rate): # 売りシグナル
    if rate['CORR'][i-1] > 0  and rate['CORR'][i] < 0:
        return True

    return False

def BuyExit(i, p, rate): # 買いポジション決済シグナル
    '''
    if rate['LOW'][i] - p < -LossCutPips*0.01:
        return True, p - LossCutPips*0.01
    if rate['HIGH'][i] - p - Spread*0.01 > ProfitPips*0.01:
        return True, p + ProfitPips*0.01
    '''
    if rate['CLOSE'][i] - p < -LossCutPips*0.01:
        return True,  rate['CLOSE'][i]
    if rate['CLOSE'][i] - p - Spread*0.01 > ProfitPips*0.01:
        return True,  rate['CLOSE'][i]


    if rate['CORR'][i-1] > CorrThreshold  and rate['CORR'][i] < CorrThreshold:
        return True, rate['CLOSE'][i]

    if SellEntry(i, rate):
        return True, rate['CLOSE'][i]
    
    return False, 0.0

def SellExit(i, p, rate): # 売りポジション決済シグナル
    '''
    if rate['HIGH'][i] - p > LossCutPips*0.01:
        return True, p + LossCutPips*0.01

    if rate['LOW'][i] - p + Spread*0.01 < -ProfitPips*0.01:
        return True, p - ProfitPips*0.01
    '''

    if rate['CLOSE'][i] - p > LossCutPips*0.01:
        return True, rate['CLOSE'][i]
    if rate['CLOSE'][i] - p + Spread*0.01 < -ProfitPips*0.01:
        return True, rate['CLOSE'][i]

    if rate['CORR'][i-1] < -CorrThreshold  and rate['CORR'][i] > -CorrThreshold:
        return True, rate['CLOSE'][i]

    if BuyEntry(i, rate):
        return True, rate['CLOSE'][i]
    
    return False, 0.0

Lots = 1.0 # 売買ロット数


def sim(rate):
    TotalProfit =0.0
    PROFIT = 0.0
    NumWin = 0
    NumLoss = 0
    LongPos = 0.0
    ShortPos =0.0
    LongNum = 0
    ShortNum = 0
    MaxConsvWin =0
    ConsvWin = 0
    MaxConsvLoss = 0
    ConsvLoss = 0
    BuyFlag = False
    SellFlag = False


    for i in range(1, len(rate)-1 ):
        if LongPos != 0.0 and BuyExit(i, LongPos, rate)[0]:
            #PROFIT = rate['CLOSE'][i] - LongPos
            PROFIT = BuyExit(i, LongPos, rate)[1] - LongPos
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE BUY {} {:.2f} {:.2f}  {:.4f}".format(rate['DATE'][i], rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0
            LongNum = 0
            if PROFIT > 0.0:
                NumWin = NumWin + 1
                ConsvWin += 1
                if ConsvWin > MaxConsvWin:
                    MaxConsvWin = ConsvWin
                ConsvLoss = 0

            else:
                NumLoss = NumLoss + 1
                ConsvWin = 0
                ConsvLoss += 1
                if ConsvLoss > MaxConsvLoss:
                    MaxConsvLoss = ConsvLoss

        if ShortPos != 0.0  and  SellExit(i, ShortPos, rate)[0]: 
            PROFIT = SellExit(i, ShortPos, rate)[1]  - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE SELL {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
            ShortNum = 0

            if PROFIT > 0.0:
                NumWin = NumWin + 1
                ConsvWin += 1
                if ConsvWin > MaxConsvWin:
                    MaxConsvWin = ConsvWin
                ConsvLoss = 0
            else:
                NumLoss = NumLoss + 1
                ConsvWin = 0
                ConsvLoss += 1
                if ConsvLoss > MaxConsvLoss:
                    MaxConsvLoss = ConsvLoss
                
                            
        if SellEntry(i, rate) and ShortNum == 0 : 
            ShortPos = rate['CLOSE'][i]
            ShortNum += 1

        if BuyEntry(i,rate) and LongNum==0: 
            LongPos = rate['CLOSE'][i]
            LongNum += 1
            BuyFlag = False            

    r  = result.Result()
    r.num_win = NumWin
    r.num_loss = NumLoss
    r.max_consv_win = MaxConsvWin
    r.max_consv_loss = MaxConsvLoss
    r.total_profit = TotalProfit

    #logger.info(rate['CLOSE'][i])
    logger.info(NumWin)
    logger.info(NumLoss)
    if NumWin != 0 and NumLoss != 0:
        logger.info(NumWin/(NumWin+NumLoss) )
    
    logger.info( MaxConsvWin)
    logger.info( MaxConsvLoss)
    logger.info(LongPos)
    logger.info(ShortPos)

    return r

if __name__ == '__main__':

    #r = rateset.getrate()
    #rate = rateset.initrate(r,"USD", "JPY")
    
    if len(sys.argv) > 1:
        ccy1=sys.argv[1]
        ccy2=sys.argv[2]
        ProfitPips = int(sys.argv[3])
        LossCutPips = int(sys.argv[4])
        Spread = float( sys.argv[5])
        term = int(sys.argv[6])
        CorrThreshold = float( sys.argv[7])


        if ccy2!='JPY':
            LossCutPips = LossCutPips/100
            ProfitPips = ProfitPips/100
            PipsRatio = PipsRatio/100
            Spread = Spread /100

        rs = rateset.RateSet()
        rs.corr_term = term

        rate = rs.getrate1d(ccy1+ccy2,1)


        r = sim(rate)
        log =ccy1+" "+ccy2+" "+"{:.4f}".format(r.total_profit) 

        #log =ccy1+" "+ccy2+" "+"{:.4f}".format(total) 
        logger.info(log)

    else:    
        #CCY = ["JPY", "USD", "GBP", "EUR", "AUD", "NZD", "CAD", "CHF", "ZAR"]
        ccylist = []
        f = open('CCYLIST.txt', 'r')
        ccylist = f.readlines()
        f.close()
        param_list = []

        for i in range(5, 11, 1):           #term
            for j in range(70, 95, 5):      #corr_thresh
                for k in range(50,400,25):              #profit
                    for p in range(10,200,10):          #losscut
                        for l in range(100,200,200):
                            param_list.append([i,j,k,p,l])

        for i in range(len(param_list)):
            for j in range(len(ccylist)):
                
                term = param_list[i][0]
                CorrThreshold = param_list[i][1]/100
                ProfitPips = param_list[i][2]
                LossCutPips = param_list[i][3]

                ccy1 = ccylist[j][0:3]
                ccy2 = ccylist[j][3:6]
                print(ccy1 + "/" + ccy2 )

                #if ccy2=='GBP' or ccy1=='GBP':
                #    LossCutPips = LossCutPips*1.5
                #    ProfitPips = ProfitPips*1.5


                Spread = 0.5
                if ccy2 not in ["JPY", "HKD", "MXN", "TRY", "ZAR"]:
                    LossCutPips = LossCutPips/100
                    ProfitPips = ProfitPips/100
                    PipsRatio = PipsRatio/100
                    Spread = Spread /100
                

                rs = rateset.RateSet()
                rs.corr_term = term
                rate = rs.getrate1d(ccy1+ccy2,1)
                #rate = rs.getrate1h(ccy1+ccy2,1)

                r = sim(rate)
                r.ccy = ccy1+ccy2
                
                r.macd_short_term = term
                r.macd_long_term = CorrThreshold

                r.target_profit = param_list[i][2]
                r.target_losscut = param_list[i][3]
                
                if ccy2 not in ["JPY", "HKD", "MXN", "TRY","ZAR"]:
                #if ccy2!='JPY':
                    r.total_profit = r.total_profit * 100

                log =ccy1+" "+ccy2+" "+"{:.4f}".format(r.total_profit) 
                
                    
                r.save("result.csv")
                logger.info(log)
        """

        for i in range(9):
            for j in range(9):
                LossCutPips = 30
                ccy1 = CCY[j]
                ccy2 = CCY[i]
                if ccy1 == ccy2:
                    continue
                if ccy1 == "GBP" or ccy2 == "GBP":
                    LossCutPips = 30
                if ccy2!='JPY':
                    LossCutPips = LossCutPips/100
                r, m, s = initrate(ccy1, ccy2)
                sim(r,m)
    """