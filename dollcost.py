import numpy as np
import pandas as pd
import techlib as tl
import math
SHORT_TERM = 12
LONG_TERM = 26
SIGNAL_TERM = 9

r = pd.read_csv('quote.csv',sep=',', \
    #names=('DATE','USD','GBP','EUR','CAD','CHF','AUD','NZD','ZAR'), \
    dtype=[('DATE','S10'), \
            ('USD','f8'),\
            ('GBP','f8'),\
            ('EUR','f8'),\
            ('CAD','f8'),\
            ('CHF','f8'),\
            ('AUD','f8'),\
            ('NZD','f8'),\
            ('ZAR','f8'),\
            ('ALL','f8'),\
            ('JPY','f8')], \
    parse_dates=True)

ccy1='USD'
ccy2='JPY'
rate = pd.DataFrame( {'DATE': [], 'CLOSE':[], 'AVG':[]} )
rate['CLOSE'] = r[ccy1]/r[ccy2]
rate['DATE'] = r['DATE']
rate['AVG'] = rate['CLOSE'].rolling(250).mean()

macd = tl.iMA(rate, SHORT_TERM) - tl.iMA(rate, LONG_TERM)
macddf = pd.DataFrame({'MACD':macd})

signal = tl.iMA( macddf, SIGNAL_TERM, applied_price='MACD')

rsi = tl.rsi(rate, 14)
macdset = pd.DataFrame({'MACD': macd, 'SIGNAL': signal, 'RSI':rsi})



def BuyEntry(i): # 買いシグナル
    if   macdset['MACD'][i-1] < macdset['SIGNAL'][i-1] \
    and macdset['MACD'][i] > macdset['SIGNAL'][i] :
    and macdset['RSI'][i] < 0.3:
        return True
    

    #if macdset['MACD'][i] <0.0 and  macdset['RSI'][i]< 0.3:
    #    return True
    #if macdset['MACD'][i] > macdset['SIGNAL'][i]:
    #    return True

def SellEntry(i): # 売りシグナル
    if  macdset['MACD'][i-1] > macdset['SIGNAL'][i-1] \
    and macdset['MACD'][i] < macdset['SIGNAL'][i]:
    and macdset['RSI'][i] > 0.7:
        return True
    
    #if  macdset['MACD'][i] > 0.0 and   macdset['RSI'][i] >0.7:
    #    return True
    #if  macdset['MACD'][i] < macdset['SIGNAL'][i]:
    #    return True
    

def BuyExit(i): # 買いポジション決済シグナル
    if macdset['RSI'][i] >0.7 and macdset['RSI'][i-1]> 0.7:
        return True
    #return SellEntry(i)
    
    if LongPos - rate['CLOSE'][i] >5.0:
        return True

def SellExit(i): # 売りポジション決済シグナル
    if macdset['RSI'][i] < 0.3 and macdset['RSI'][i-1] < 0.3:
        return True
    #return BuyEntry(i)
    
    if ShortPos - rate['CLOSE'][i] <-5.0:
        return True

#LongPos = pd.Series(0.0, index=rate.index) # 買いポジション情報
#ShortPos = LongPos.copy() # 売りポジション情報
Lots = 1.0 # 売買ロット数

#LongPos = pd.DataFrame(colums['DATE','RATE','PROFIT'])
#ShortPos = pd.DataFrame()


LongPos = 0.0
LongAvg = 0.0
ShortPos =0.0
ShortAvg = 0.0
TotalProfit =0.0
LongTotal = 0.0
ShortTotal = 0.0
PROFIT = 0.0
DROWDOWN = 1000000.0

#for i in range( LONG_TERM+SIGNAL_TERM+1+220, len(rate)-1 ):
for i in range( 250, len(rate)-1 ):
    
    curretProfit = 0
    if LongPos != 0.0:
        curretProfit = ( rate['CLOSE'][i] - LongAvg ) * LongPos

    if ShortPos != 0.0:
        curretProfit += -( rate['CLOSE'][i] - ShortAvg ) * ShortPos
    
    if curretProfit + TotalProfit < DROWDOWN:
        DROWDOWN =  curretProfit

    if LongPos != 0.0 and  rate['CLOSE'][i]/LongAvg>1.05:
       
        PROFIT = ( rate['CLOSE'][i] - LongAvg ) * LongPos
        TotalProfit = TotalProfit + PROFIT
        print("Long {} {:.2f} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], LongAvg, LongPos, PROFIT) )
        LongPos = 0.0
        LongAvg = 0.0
        LongTotal = 0.0
        continue

    if ShortPos != 0.0 and  rate['CLOSE'][i]/ShortAvg<0.95:
       
        PROFIT = -( rate['CLOSE'][i] - ShortAvg ) * ShortPos
        TotalProfit = TotalProfit + PROFIT
        print("Short {} {:.2f} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortAvg, ShortPos, PROFIT) )
        ShortPos = 0.0
        ShortAvg = 0.0
        ShortTotal=0.0
        continue
    
    if BuyEntry(i) : 
        n = round( 1/math.log(rate['CLOSE'][i] ,130), 1)
        n = 1.0
        if LongAvg != 0:
            n = LongAvg / rate['CLOSE'][i]
        LongPos += n
        LongTotal += rate['CLOSE'][i]*n
        LongAvg = LongTotal/LongPos

        #ShortPos += n/2
        #ShortTotal += rate['CLOSE'][i]*n/2
        #ShortAvg = ShortTotal/ShortPos


    if SellEntry(i) : 
        n = round( math.log(rate['CLOSE'][i],130) ,1)
        n =1.0
        if ShortAvg != 0:
            n = rate['CLOSE'][i] / ShortAvg 
        ShortPos += n
        ShortTotal += rate['CLOSE'][i]*n
        ShortAvg = ShortTotal/ShortPos

        #LongPos += n/2
        #LongTotal += rate['CLOSE'][i]*n/2
        #LongAvg = LongTotal/LongPos

        #print(ShortAvg)


PROFIT = ( rate['CLOSE'][i] - LongAvg ) * LongPos
TotalProfit = TotalProfit + PROFIT
PROFIT = -( rate['CLOSE'][i] - ShortAvg ) * ShortPos
TotalProfit = TotalProfit + PROFIT



print('Total Profit {:.2f} {:.2f}'.format(TotalProfit, DROWDOWN) )
