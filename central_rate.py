import urllib.request
import mysql.connector as mydb
import glob
import csv
import os
 
class CentralRate:
    Pair_List = ["01_USDJPY_D.csv","02_EURJPY_D.csv","03_EURUSD_D.csv","04_GBPJPY_D.csv","05_GBPUSD_D.csv","06_CADJPY_D.csv","07_USDCAD_D.csv","08_AUDJPY_D.csv","09_AUDUSD_D.csv","10_USDCHF_D.csv","11_NZDUSD_D.csv","12_EURGBP_D.csv","13_NZDJPY_D.csv","14_CHFJPY_D.csv","15_EURCHF_D.csv","16_ZARJPY_D.csv","18_GBPCHF_D.csv","20_AUDCHF_D.csv","21_AUDNZD_D.csv","22_NZDCHF_D.csv","23_CNHJPY_D.csv","24_EURAUD_D.csv","26_TRYJPY_D.csv","27_GBPAUD_D.csv","28_MXNJPY_D.csv"]
    URL = "https://info.ctfx.jp/service/market/csv/"


    def download(self):
        for csv in pairlist: 
        # ダウンロードする
            mem = urllib.request.urlopen(url+csv).read()
 
            # ファイルへの保存
            with open("./rate/central_daily/"+csv, mode="wb") as f:
                f.write(mem)

    def insert_db(self):
        conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx'
        )

        # コネクションが切れた時に再接続してくれるよう設定
        conn.ping(reconnect=True)

        # 接続できているかどうか確認
        print(conn.is_connected())
        cur = conn.cursor()
        for filename in glob.glob("/mnt/c/rate/central_daily/*.csv"):
            records = []
            with open(filename, encoding="shift_jisx0213") as f:
                for r in csv.reader(f):
                    r[0] = "00"+r[0]
                    r[0] = r[0][-2:]
                    
                    ccy = self.get_ccy(r[0])

                    r[1] = r[1][0:4]+r[1][5:7]+r[1][8:10] #日付のフォーマット変換
                    records.append([ccy,r[1],"000000",r[2],r[3],r[4],r[5]])

                records.pop(0) #ヘッダー削除
                #print(records[0][0])

                #<TICKER>,<DTYYYYMMDD>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>
                cur.executemany("INSERT INTO rate1d VALUES (%s, %s, %s, %s, %s, %s, %s)", records)
                conn.commit()

    def get_ccy(self, cd):
        for pair in self.Pair_List:
            if cd == pair[0:2]:
                return pair[3:9]
        return None


if __name__ == '__main__':
    r =  CentralRate()
    r.insert_db()
    #print(r.get_ccy("01"))
