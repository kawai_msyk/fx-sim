import mysql.connector as mydb
import csv
import sys
import pickle

class Result:
    def __init__(self):
        self.ccy = ""
        self.num_win = 0
        self.num_loss = 0
        self.max_consv_win = 0
        self.max_consv_loss = 0
        self.macd_short_term = 0
        self.macd_long_term = 0
        self.macd_signal_term = 0
        self.total_profit = 0
        self.target_profit = 1000000
        self.target_losscut = 1000000

        self.open_threshold = 0
        self.close_threshold = 0

        self.param = []

        self.result_file = ""
        self.profit_position  = 6
        self.ccy_position = 0
        self.result_corr = 0
        self.from_date = ""
        self.to_date = ""
        self.rate_term =""

    def save(self, file_name):
        line=[]
        #f = open(file_name, 'a')

        line.append( self.from_date)
        line.append( self.to_date)
        line.append( self.rate_term)

        line.append( self.ccy )
        line.append( round( self.total_profit,2) )
        line.append( self.num_win )
        line.append( self.num_loss )
        line.append( self.max_consv_win )
        line.append( self.max_consv_loss )
        #line.append( self.profit_std)

        for p in self.param:
            line.append( p )

        print( str(line) )
        #f.write(str(line))
        with open( file_name, 'a', newline="") as f:
            writer = csv.writer(f)
            writer.writerow(line)


    def insert_db(self, fname, id):
        conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx'
        )

        # コネクションが切れた時に再接続してくれるよう設定
        conn.ping(reconnect=True)
        # 接続できているかどうか確認
        print(conn.is_connected())
        cur = conn.cursor()
        with open(fname) as f:
            records = []
            cnt = 0
            for r in csv.reader(f):
                r.append("")
                r.insert(0,id)
                #$print(r)
                records.append(r)
                cnt = cnt + 1
                #print(records)

                if cnt % 10000 == 0:
                    cur.executemany("INSERT INTO result_macd VALUES (%s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)", records)
                    conn.commit()
                    records = []
                    print(cnt)

            cur.executemany("INSERT INTO result_macd VALUES (%s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)", records)
            conn.commit()

    def extract_freq_all(self):
        ccylist = []
        f = open('CCYLIST.txt', 'r')
        ccylist = f.readlines()
        f.close()

        for ccy in ccylist:
            self.extract_freq(ccy[0:6])



    def extract_freq(self, ccy):
        conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx'
        )

        # コネクションが切れた時に再接続してくれるよう設定
        conn.ping(reconnect=True)

        # 接続できているかどうか確認
        #print(conn.is_connected())
        cur = conn.cursor()
        cur.execute("SELECT * FROM result_macd where id =4 and CCY = %s ORDER BY Profit Desc Limit 20", [ccy])
        rows =  cur.fetchall()
        #print(rows)
        #r = []
        short_term = {}
        long_term = {}
        signal_term = {}
        for row in rows:
            if row[1] in short_term:
                cnt =  short_term[ row[1] ]
                cnt = cnt + 1
            else:
                cnt = 1
            short_term[ row[1] ] = cnt

            if row[2] in long_term:
                cnt =  long_term[ row[2] ]
                cnt = cnt + 1
            else:
                cnt = 1
            long_term[ row[2] ] = cnt

            if row[3] in signal_term:
                cnt =  signal_term[ row[3] ]
                cnt = cnt + 1
            else:
                cnt = 1
            signal_term[ row[3] ] = cnt

        mode_short = sorted(short_term.items(), key=lambda x:x[1], reverse=True)[0][0]
        mode_long = sorted(long_term.items(), key=lambda x:x[1], reverse=True)[0][0]
        mode_signal =  sorted(signal_term.items(), key=lambda x:x[1], reverse=True)[0][0]

        print('{ "CCY":"' + ccy + '"', ',"short_term":', mode_short, ',"long_term":', mode_long, ',"signal_term":', mode_signal,'}')

        #print('{ "CCY": ""+ccy+"",""", "short_term:", mode_short,"', ""long_term"":", mode_long, "', ""signal_term':"", mode_signal, "}")

        return mode_short, mode_long, mode_signal
        #print(ccy,"\n",sorted(short_term.items(), key=lambda x:x[1], reverse=True), "\n",
        #    sorted(long_term.items(), key=lambda x:x[1], reverse=True), "\n",
        #    sorted(signal_term.items(), key=lambda x:x[1], reverse=True))


    def extract_best_all(self):
        ccylist = []
        f = open('CCYLIST.txt', 'r')
        ccylist = f.readlines()
        f.close()

        for ccy in ccylist:
            self.extract_best(ccy[0:6])


    def extract_best_all_from_file(self):
        ccyset = {"USDJPY"} #initalized set.If you don't set an initial value it will be considered a dictionary.


        with open( self.result_file) as f:
            reader = csv.reader(f)
            for row in reader:
                if len(row) < 1:
                    continue

                ccy = row[self.ccy_position]
                if ccy not in ccyset:
                    ccyset.add(ccy)

        total = 0
        for ccy in sorted(ccyset):
            p, m = self.extract_best_from_file(ccy[0:6])
            total = total + m

        print(round(total,2))

    def extract_best_from_file(self, ccy):
        max = -100
        param = []
        with open( self.result_file) as f:

            reader = csv.reader(f)
            for row in reader:
                #print(row)
                if len(row) < 1:
                    continue
                if row[self.ccy_position] != ccy:
                    continue

                if float(row[self.profit_position]) > max:
                    max =  float(row[self.profit_position])
                    param = row

        result_str = str(param) + "," + str(max)
        result_str = result_str.replace("[","")
        result_str = result_str.replace("]","")
        result_str = result_str.replace("'","")
        print(result_str)
        #print(param, max)

        return param,max


    def extract_top_from_file(self, ccy):
        max = -100
        param = []
        with open( self.result_file) as f:

            reader = csv.reader(f)
            for row in reader:
                #print(row)
                if len(row) < 1:
                    continue
                if row[self.ccy_position] != ccy:
                    continue

                if float(row[self.profit_position]) > max:
                    max =  float(row[self.profit_position])
                    param = row

        result_str = str(param) + "," + str(max)
        result_str = result_str.replace("[","")
        result_str = result_str.replace("]","")
        result_str = result_str.replace("'","")
        print(result_str)
        #print(param, max)

        return param,max






    def extract_best(self, ccy):
        conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx'
        )

        # コネクションが切れた時に再接続してくれるよう設定
        conn.ping(reconnect=True)

        # 接続できているかどうか確認
        #print(conn.is_connected())
        cur = conn.cursor()

        sql = "SELECT a.ccy, a.short_term,a.long_term, a.signal_term, a.profit " \
                "FROM `result_macd`  as a "\
                "INNER JOIN ( " \
                    "select id ,ccy, Max(profit) as mp from result_macd "\
                    "group by id , ccy "\
                    ") as b " \
                "on a.ccy = b.ccy " \
                "and a.profit = b.mp " \
                "and a.id = b.id " \
                "where a.id=17 and a.CCY =%s"

        cur.execute(sql, [ccy])
        row =  cur.fetchone()
        #print(row)
        #r = []
        short_term = row[1]
        long_term = row[2]
        signal_term = row[3]


        print('{ "CCY":"' + ccy + '"', ',"short_term":', short_term, ',"long_term":', long_term, ',"signal_term":', signal_term,'},')

        return short_term, long_term, signal_term


if __name__ == '__main__':
    r = Result()


    r.result_file = sys.argv[1]
    r.ccy_position = 3
    r.profit_position = 4

    #r.ccy_position = int(sys.argv[2])
    #r.profit_position = int(sys.argv[3])
    #r.extract_best_from_file("GBPJPY")

    r.extract_best_all_from_file()

    #r.insert_db(sys.argv[1], sys.argv[2])

    #r.extract_freq_all()

    #r.extract_best_all()

    #r.ccy = "USDJPY"
    #r.save("result.csv")
