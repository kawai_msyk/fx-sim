import numpy as np
import pandas as pd
import techlib as tl
import sys
import logging
import glob
import mysql.connector as mydb
import csv
import json
import copy
from azure.cosmosdb.table.tableservice import TableService
from azure.cosmosdb.table.models import Entity

import rci05
import rateset



class SignalAlert:
    STORAGE_NAME = 'storageaccountrg03b52e'
    STORAGE_KEY = 'cIoDGio95MAFBWCypz8ZZjRWNSItD4D6NgvouTfrPB+3lAmbu3WWFmnmXZcJkBfdBNnc1Nj54W4THls1+vs0bg=='
    table_service = TableService(account_name=STORAGE_NAME, account_key=STORAGE_KEY)
    #p=table_service.get_table_service_properties()
    #print("Parameter",p)
    #table_service.set_table_service_properties(logging=None, hour_metrics=None, minute_metrics=None, cors=None, timeout=None)
    logger = logging.getLogger('azure')
    logger.setLevel(logging.ERROR)
    def check_signal(self):
        rs = rateset.RateSet()
        rs.from_date = "20220401"
        rs.to_date = "20220930"
        ccylist = []

        f = open('CCYLIST.txt', 'r')
        ccylist = f.readlines()
        f.close()

        result_lis = []
        for c in copy.copy(ccylist):
            if c[0:1] == "#":
                ccylist.remove(c)


        for ccy in ccylist:
            #print(ccy)
            target_ccy = ccy.split(",")[0]
            rs.rci_term01 = int( ccy.split(",")[1] )
            rs.rci_term02 = int( ccy.split(",")[2] )
            rs.rci_term03 = int( ccy.split(",")[3] )
            rs.rci_term04 = int( ccy.split(",")[4] )

            rci05.ProfitPips = int( ccy.split(",")[5] )
            rci05.LossCutPips = int( ccy.split(",")[6] )

            #print(target_ccy, rci05.ProfitPips, rci05.LossCutPips)
            
            if target_ccy[3:6] not in ["JPY", "HKD", "MXN", "TRY", "ZAR"]:
                rci05.LossCutPips = rci05.LossCutPips/100
                rci05.ProfitPips = rci05.ProfitPips/100
            
            #print(target_ccy, rci05.ProfitPips, rci05.LossCutPips)
            rate_list = rs.getrate1d(target_ccy, 1)
            #rate = rs.getrate10m(ccy1+ccy2,1)
            #rate = rs.getrate8h(ccy1+ccy2,1)
            #rate = rs.getrate4h(ccy1+ccy2,1)

            r = rs.init_dataframe(rate_list,1)

            #print(rate)
            rate = r[ r['DATE']>'20220829' ]
            rate.reset_index(inplace=True, drop=True)

            #print(rate)
            #print( rate[ rate['DATE']>'20220901' ] )

            longPosi = None
            shortPosi = None
            positionList = self.getPosition(target_ccy)
            for p in positionList:
                #print(p)
                if p.BuySell == "Buy":
                    longPosi = p.PositionRate
                if p.BuySell == "Sell":
                    shortPosi = p.PositionRate


            for i in range( len(rate)-2, len(rate)-1 ): 
            #for i in range(1, len(rate)-1 ):
                if shortPosi is not None and rci05.SellExit(i, shortPosi, rate)[0] == True:
                    print("SellExit",rate['CCY'][i],rate['DATE'][i],rate['CLOSE'][i])
                    shortPosi = None
                
                if longPosi is not None and  rci05.BuyExit(i, longPosi, rate)[0] == True:
                    print("BuyExit",rate['CCY'][i],rate['DATE'][i],rate['CLOSE'][i])
                    longPosi = None

                if rci05.SellEntry(i, rate) == True:
                    print("Sell",rate['CCY'][i],rate['DATE'][i],rate['CLOSE'][i])
                    shortPosi = rate['CLOSE'][i]

                if rci05.BuyEntry(i, rate) == True:
                    print("Buy",rate['CCY'][i],rate['DATE'][i],rate['CLOSE'][i])
                    longPosi = rate['CLOSE'][i]

    def getPosition(self, ccy):
        posi = self.table_service.query_entities( 'position', filter="PartitionKey eq '" +ccy+"'")
        for p in posi:
            print(p.PartitionKey, p.BuySell, p.RowKey, p.PositionRate)
        return posi

if __name__ == '__main__':
    sa = SignalAlert()
    sa.check_signal()

