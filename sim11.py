import numpy as np
import pandas as pd
import techlib as tl
import rateset
import sys
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


SHORT_TERM = 12
LONG_TERM = 26
SIGNAL_TERM = 9
LossCutPips = 10
ProfitPips = 100

def BuyEntry(i, rate): # 買いシグナル
    cnt = 0
    
    for j in range(4):
        if rate['CLOSE'][i-j]-rate['OPEN'][i-j] < 0.0:
            cnt = cnt + 1

    if cnt == 4:
        return True
    else:
        False

def SellEntry(i, rate): # 売りシグナル
    cnt = 0
    for j in range(4):
        if rate['CLOSE'][i-j]-rate['OPEN'][i-j] > 0.0:
            cnt = cnt + 1

    if cnt == 4:
        return True
    else:
        False

def BuyExit(i, p, rate): # 買いポジション決済シグナル
    #if rate['CLOSE'][i] - p < -LossCutPips*0.01:
    #    return True, p-LossCutPips*0.01

    if rate['LOW'][i] - p < -LossCutPips*0.01:
        return True, p - LossCutPips*0.01

    if rate['HIGH'][i] - p > ProfitPips*0.01:
        return True, p + ProfitPips*0.01

    #if SellEntry(i, rate):
    #    return True, rate['CLOSE'][i]

    return False, 0.0

def SellExit(i, p, rate): # 売りポジション決済シグナル
    #if rate['CLOSE'][i] - p > LossCutPips*0.01:
    #    return True, p + LossCutPips*0.01

    if rate['HIGH'][i] - p < -LossCutPips*0.01:
        return True, p + LossCutPips*0.01

    if rate['LOW'][i] - p < -ProfitPips*0.01:
        return True, p - ProfitPips*0.01

    #if BuyEntry(i, rate):
    #    return True, rate['CLOSE'][i]

    return False, 0.0

Lots = 1.0 # 売買ロット数


def sim(rate):
    LongPos = 0.0
    ShortPos =0.0
    TotalProfit =0.0
    PROFIT = 0.0
    for i in range( 60, len(rate)-1 ):
        if LongPos != 0.0 and BuyExit(i, LongPos, rate)[0]:
            PROFIT = BuyExit(i, LongPos, rate)[1] - LongPos
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE BUY {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0

        if ShortPos != 0.0 and  SellExit(i, ShortPos, rate)[0]: 
            PROFIT = SellExit(i, ShortPos, rate)[1]  - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE SELL {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
        
        if BuyEntry(i,rate) and LongPos == 0.0: 
            LongPos = rate['CLOSE'][i]
                
        if SellEntry(i, rate) and ShortPos == 0.0: 
            ShortPos = rate['CLOSE'][i]
    
    return TotalProfit

if __name__ == '__main__':

    r = rateset.getrate()
    rate = rateset.initrate(r,"USD", "JPY")
    if len(sys.argv) > 1:
        ccy1=sys.argv[1]
        ccy2=sys.argv[2]
        if ccy2!='JPY':
            LossCutPips = LossCutPips/100

        rate = rateset.initrate(r,ccy1, ccy2)
        total = sim(rate)
        log =ccy1+" "+ccy2+" "+"{:.4f}".format(total)
        logger.info(log)

    '''
    else:    
        CCY = ["JPY", "USD", "GBP", "EUR", "AUD", "NZD", "CAD", "CHF", "ZAR"]

        for i in range(9):
            for j in range(9):
                LossCutPips = 30
                ccy1 = CCY[j]
                ccy2 = CCY[i]
                if ccy1 == ccy2:
                    continue
                if ccy1 == "GBP" or ccy2 == "GBP":
                    LossCutPips = 30
                if ccy2!='JPY':
                    LossCutPips = LossCutPips/100
                r, m, s = initrate(ccy1, ccy2)
                sim(r,m)

    '''