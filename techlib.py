import numpy as np
import pandas as pd


def macd(df, shartterm, longterm, signalterm, ma_shift=0, ma_method='EMA', applied_price='CLOSE'):
    macd = iMA(rate, shortterm) - iMA(rate, longterm)
    macddf = pd.DataFrame({'MACD':macd})
    signal = iMA( macddf, signalterm, applied_price='MACD')
    return  pd.DataFrame({'MACD': macd, 'SIGNAL': signal})

def rsi(df, term,  applied_price='CLOSE'):
    _diff = df[applied_price].diff(1)
    _posi = _diff.clip_lower(0).ewm(alpha=1/term).mean()
    _nega = _diff.clip_upper(0).ewm(alpha=1/term).mean()
    return _posi / (_posi - _nega)    



def iMA(df, ma_period, ma_shift=0, ma_method='SMA', applied_price='CLOSE'):
    if ma_method == 'SMA':    
        #return df[applied_price].rolling(window=ma_period).mean().shift(ma_shift)
        return df[applied_price].rolling(window=ma_period).mean()
    elif ma_method == 'EMA':
        return df[applied_price].ewm(span=ma_period).mean().shift(ma_shift)
    elif ma_method == 'SMMA':
        return df[applied_price].ewm(alpha=1/ma_period).mean().shift(ma_shift)
    elif ma_method == 'LWMA':
        y = pd.Series(0.0, index=df.index)
        for i in range(len(y)):
            if i<ma_period-1: y[i] = 'NaN'
            else:
                y[i] = 0
                for j in range(ma_period):
                    y[i] += df[applied_price][i-j]*(ma_period-j)
                y[i] /= ma_period*(ma_period+1)/2
        return y.shift(ma_shift)
    else: return df[applied_price].copy().shift(ma_shift)


def stdev(df, ma_period, ma_shift=0,  applied_price='CLOSE'):
    return df[applied_price].rolling(ma_period).std()
