# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 18:14:53 2017
@author:
"""
import os
import shutil
import time
import datetime
import requests
import zipfile

baseurl = 'https://www.forexite.com/free_forex_quotes/'
target_directory = './rate/1m/'

def check_dir():
    if os.path.exists(target_directory) == False:
        os.mkdir(target_directory)

def get_filelist():
    START_DATE = datetime.datetime(2023, 3, 31)
    END_DATE = datetime.datetime(2023, 7, 28)
    today = datetime.datetime.today()
    #END_DATE = today
    date_list = []
    get_date = START_DATE
    while get_date < END_DATE:
        year = "{0:04d}".format(get_date.year)
        year2 = year[2:]
        month = "{0:02d}".format(get_date.month)
        day = "{0:02d}".format(get_date.day)
        filepath = year + "/" + month + "/" + day + month + year2 + ".zip"
        date = year + month + day
        datedata = {'date':date, 'filepath':filepath}
        date_list.append(datedata)
        get_date += datetime.timedelta(days=1)
    return date_list

def download_file(url):
    filename = url.split('/')[-1]
    res = requests.get(url, stream=True)
    print(url)
    content_type = res.headers['content-type']
    if res.status_code == 200 and content_type == 'application/zip':
        with open(filename, 'wb') as f:
            for chunk in res.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
                    f.flush()
            return filename
        # ファイルが開けなかった場合は False を返す
        return False

def move_file(filename):
    if os.path.exists(filename) == True:
        shutil.move(filename, target_directory + filename)
        return target_directory + filename
    else:
        return None

def zip_extract(filename, date):
    with zipfile.ZipFile(filename, 'r') as f:
        filelist = f.namelist()

    zfile = zipfile.ZipFile(filename)
    zfile.extractall(target_directory)

    for info in filelist:
        try:
            #file_path = target_directory + info, target_directory + date + '.txt'
            file_path = target_directory + date + '.txt'
            if os.path.isfile( file_path ):
                os.remove(file_path)

            os.rename(target_directory + info, target_directory + date + '.txt')
        except:
            return

if __name__ == '__main__':
    check_dir()
    filelist = get_filelist()
    for filedata in filelist:
        filepath = filedata['filepath']
        filename = download_file(baseurl + filepath)
        if filename != False and filename != None:
            time.sleep(1)
            filepath = move_file(filename)
            if filepath != None:
                zip_extract(filepath, filedata['date'])
