import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pylab as plt
import seaborn as sns


from sklearn import linear_model

def macd( r, term1, term2):
    s = r[0:term1].sum()
    l = r[0:term2].sum()

    s = s + r[term1-1]
    l = l + r[term2-1]

    s = s / (term1+1)
    l = l / (term2+1)

    return s-l



def rsi( data, term):
    up = 0.0
    down = 0.0
    for i in range(0, term):
        if data[i] < data[i+1]:
            up += data[i+1] - data[i]
        else:
            down += data[i] - data[i+1]
    return up/(up+down)


data_dir = "./"
#レート読み込み　日付昇順
rate = np.loadtxt(data_dir + "cadjpy.csv", delimiter=",",  usecols=(1) ) 

day_ago = 25
rsi_num = 11
rsi_term = 14
sig_term = 9
avg_term = 21
corr_term = 7

avglist = np.zeros( len(rate) )
macdlist = np.zeros( len(rate) )
signallist = np.zeros( len(rate) )
corrlist = np.zeros( len(rate) )

for i in range(38, len(rate) ):
    macdlist[i] = ( macd(rate[i-26:i], 12, 26) ) 
    avglist[i] = rate[i-avg_term:i].mean()

for i in range(38, len(rate) ):
    signallist[i] = macdlist[i-sig_term:i].mean()

for i in range(38, len(rate) ):
    #r1 = rate[i-corr_term : i]
    r1 = avglist[ i-corr_term : i]
    r2 = macdlist[i-corr_term : i]
    corrlist[i] = ( np.corrcoef(r1,r2)[0, 1] )

#print(corrlist)


profit = 0.0
losscut = 0.1

order = np.zeros( len(rate) )
profitlist = np.zeros( len(rate) )

for i in range(38, len(rate)-1):
    p = rate[i+1] - rate[i]

    if corrlist[i]> 0.8:## and  corrlist[i] > corrlist[i-1]:
        if macdlist[i] > signallist[i]:
            profit += p if p > -losscut else -losscut
            order[i] = 1

        else:
            profit += -p if p < losscut else -losscut
            order[i] = -1


    if corrlist[i]< -0.8:## and  corrlist[i] < corrlist[i-1]:
        if macdlist[i] > signallist[i]:
            profit += -p if p < losscut else -losscut
            order[i] = -1
        else:
            profit += p if p > -losscut else -losscut
            order[i] = 1

    profitlist[i+1] = profit


result = np.vstack([rate, avglist, macdlist, signallist, corrlist, profitlist, order])
np.savetxt("result.csv", result.T, delimiter=",", fmt= ['%.2f','%.2f','%.2f','%.2f','%.2f','%.2f','%d'] )


print(profit)
