import numpy as np
import pandas as pd
import techlib as tl
import rateset
import result
import sys
import logging
import copy
import statistics

logger = logging.getLogger(__name__)
handler1 = logging.FileHandler(filename=sys.argv[8])  #handler2はファイル出力
handler1.setLevel(logging.INFO)     #handler2はLevel.WARN以上
logger.addHandler(handler1)

LossCutPips = 5
ProfitPips = 45
PipsRatio = 1.0

Spread = 0.5
BuyFlag = False
SellFlag = False

BuyRate = 0.0
SellRate = 0.0
JudgeCnt = 0

RCIThreshold01 = 0.9
RCIThreshold02 = 0.9


def BuyEntry(i, rate): # 買いシグナル
    if rate['MACD'][i-1] < rate['SIGNAL'][i-1]  and rate['MACD'][i] > rate['SIGNAL'][i]:
        return True

    return False

def SellEntry(i, rate): # 売りシグナル
    if rate['MACD'][i-1] > rate['SIGNAL'][i-1]  and rate['MACD'][i] < rate['SIGNAL'][i]:
        return True

    return False

def BuyExit(i, p, rate): # 買いポジション決済シグナル
    if rate['CLOSE'][i] - p < -LossCutPips*0.01:
        return True,  rate['CLOSE'][i]
    if rate['CLOSE'][i] - p - Spread*0.01 > ProfitPips*0.01:
        return True,  rate['CLOSE'][i]

    if rate['RCI_TERM01'][i-1] > RCIThreshold01 and rate['RCI_TERM01'][i] < RCIThreshold01:
        return True, rate['CLOSE'][i]

    if rate['RCI_TERM01'][i-1] > rate['RCI_TERM02'][i-1]  and rate['RCI_TERM01'][i] < rate['RCI_TERM02'][i]:
        return True, rate['CLOSE'][i]


    if SellEntry(i, rate):
        return True, rate['CLOSE'][i]

    return False, 0.0


def SellExit(i, p, rate): # 売りポジション決済シグナル
    if rate['CLOSE'][i] - p > LossCutPips*0.01:
        return True, rate['CLOSE'][i]
    if rate['CLOSE'][i] - p + Spread*0.01 < -ProfitPips*0.01:
        return True, rate['CLOSE'][i]

    if rate['RCI_TERM01'][i-1] < -RCIThreshold01 and rate['RCI_TERM01'][i] > -RCIThreshold01:
        return True, rate['CLOSE'][i]


    if rate['RCI_TERM01'][i-1] < rate['RCI_TERM02'][i-1]  and rate['RCI_TERM01'][i] > rate['RCI_TERM02'][i]:
        return True,  rate['CLOSE'][i]


    if BuyEntry(i, rate):
        return True, rate['CLOSE'][i]

    return False, 0.0

Lots = 1.0 # 売買ロット数


def sim(rate, from_idx, to_idx):
    TotalProfit =0.0
    PROFIT = 0.0
    NumWin = 0
    NumLoss = 0
    LongPos = 0.0
    ShortPos =0.0
    LongNum = 0
    ShortNum = 0
    MaxConsvWin =0
    ConsvWin = 0
    MaxConsvLoss = 0
    ConsvLoss = 0
    BuyFlag = False
    SellFlag = False

    profit_list = []

    #logging.basicConfig(level=logging.DEBUG)
    for i in range(1, len(rate)-1 ):
        if i  < from_idx or i > to_idx:
            continue

        if LongPos != 0.0 and BuyExit(i, LongPos, rate)[0]:
            #PROFIT = rate['CLOSE'][i] - LongPos
            PROFIT = BuyExit(i, LongPos, rate)[1] - LongPos
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE BUY {} {:.2f} {:.2f}  {:.4f}".format(rate['DATE'][i], rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0
            LongNum = 0
            if PROFIT > 0.0:
                NumWin = NumWin + 1
                ConsvWin += 1
                if ConsvWin > MaxConsvWin:
                    MaxConsvWin = ConsvWin
                ConsvLoss = 0

            else:
                NumLoss = NumLoss + 1
                ConsvWin = 0
                ConsvLoss += 1
                if ConsvLoss > MaxConsvLoss:
                    MaxConsvLoss = ConsvLoss

        if ShortPos != 0.0  and  SellExit(i, ShortPos, rate)[0]:
            PROFIT = SellExit(i, ShortPos, rate)[1]  - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE SELL {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
            ShortNum = 0

            if PROFIT > 0.0:
                NumWin = NumWin + 1
                ConsvWin += 1
                if ConsvWin > MaxConsvWin:
                    MaxConsvWin = ConsvWin
                ConsvLoss = 0
            else:
                NumLoss = NumLoss + 1
                ConsvWin = 0
                ConsvLoss += 1
                if ConsvLoss > MaxConsvLoss:
                    MaxConsvLoss = ConsvLoss


        if SellEntry(i, rate) and ShortNum == 0 :
            ShortPos = rate['CLOSE'][i]
            ShortNum += 1

        if BuyEntry(i,rate) and LongNum==0:
            LongPos = rate['CLOSE'][i]
            LongNum += 1
            BuyFlag = False

        profit_list.append(PROFIT)

    r  = result.Result()
    r.num_win = NumWin
    r.num_loss = NumLoss
    r.max_consv_win = MaxConsvWin
    r.max_consv_loss = MaxConsvLoss
    r.total_profit = TotalProfit
    #r.profit_std = statistics.pstdev(profit_list)

    #profit_std = statistics.pstdev(profit_list)
    #profit_mean = statistics.mean(profit_list)

    #for i in range(1, len(rate)-1 ):


    #logger.info(rate['CLOSE'][i])

    #logger.info(NumWin)
    #logger.info(NumLoss)
    #if NumWin != 0 and NumLoss != 0:
    #    logger.info(NumWin/(NumWin+NumLoss) )

    #logger.info( MaxConsvWin)
    #logger.info( MaxConsvLoss)
    #logger.info(LongPos)
    #logger.info(ShortPos)

    return r

if __name__ == '__main__':

    #r = rateset.getrate()
    #rate = rateset.initrate(r,"USD", "JPY")

    if len(sys.argv) > 9:
        ccy1=sys.argv[1]
        ccy2=sys.argv[2]
        ProfitPips = int(sys.argv[3])
        LossCutPips = int(sys.argv[4])
        Spread = float( sys.argv[5])
        term1 = int(sys.argv[6])
        term2 = int(sys.argv[7])
        RCIThreshold01 = float( sys.argv[8])
        RCIThreshold02 = float( sys.argv[9])


        if ccy2!='JPY':
            LossCutPips = LossCutPips/100
            ProfitPips = ProfitPips/100
            PipsRatio = PipsRatio/100
            Spread = Spread /100

        rs = rateset.RateSet()
        rs.rci_short_term = term1
        rs.rci_long_term = term2

        #rate = rs.getrate1d(ccy1+ccy2,1)
        #rate = rs.getrate1h(ccy1+ccy2,1)
        rate = rs.getrate10m(ccy1+ccy2,1)
        #rate = rs.getrate8h(ccy1+ccy2,1)

        #print(rate)
        r = sim(rate)
        log =ccy1+" "+ccy2+" "+"{:.4f}".format(r.total_profit)

        #log =ccy1+" "+ccy2+" "+"{:.4f}".format(total)
        logger.info(log)

    else:
        start_idx = int(sys.argv[1])
        end_idx = int(sys.argv[2])
        skip_term = int(sys.argv[3])
        #result_file = sys.argv[6]

        #CCY = ["JPY", "USD", "GBP", "EUR", "AUD", "NZD", "CAD", "CHF", "ZAR"]
        ccylist = []
        f = open('CCYLIST.txt', 'r')
        ccylist = f.readlines()
        f.close()

        for c in copy.copy(ccylist):
            if c[0:1] == "#":
                ccylist.remove(c)

        print(ccylist)

        param_list = []
        prev_ccy=""
        for t1 in range(start_idx, end_idx, skip_term):           #term1
            for t2 in range(t1+3, end_idx+5, skip_term):      #term2
                for th1 in range(70, 100, 5):      #term2
                    for p in range(100,400,50):          #profit
                        for l in range(100,400,50):        #losscut
                            param_list.append([t1,t2,th1,p,l])

        rs = rateset.RateSet()
        rs.from_date = sys.argv[4]
        rs.to_date = sys.argv[5]

        ph1 = int(sys.argv[6])
        ph2 = int(sys.argv[7])
        for j in range(len(ccylist)):

            ccy1 = ccylist[j][0:3]
            ccy2 = ccylist[j][3:6]
            print(ccy1 + "/" + ccy2 )

            rate_list_org = rs.getrate1d(ccy1+ccy2,1) #20210101-20220630
            #rate_list_org = rs.getrate8h(ccy1+ccy2,1) #20220101-20220630
            #rate_list_org = rs.getrate4h(ccy1+ccy2,1) #20220101-20220630
            #rate_list_org = rs.getrate1h(ccy1+ccy2,1) #20220101-20220630
            #rate_list_org = rs.getrate15m(ccy1+ccy2,1) #20210101-20220630
            for k in range(0, len(rate_list_org)-ph1-ph2, ph2) :
                t1_max = 0
                t2_max = 0
                p_max = 0
                l_max = 0
                th1_max =0
                max_profit = -100
                for i in range(len(param_list)):

                    term1 = param_list[i][0]
                    term2 = param_list[i][1]

                    RCIThreshold01 = param_list[i][2]
                    ProfitPips = param_list[i][3]
                    LossCutPips = param_list[i][4]

                    PipsRatio = 1.0
                    Spread = 0.5
                    if ccy2 not in ["JPY", "HKD", "MXN", "TRY", "ZAR"]:
                        LossCutPips = LossCutPips/100
                        ProfitPips = ProfitPips/100
                        PipsRatio = PipsRatio/100
                        Spread = Spread /100

                    rs.rci_term01 = term1
                    rs.rci_term02 = term2

                    prev_ccy = ccy1+ccy2
                    rate_list = copy.deepcopy(rate_list_org)
                    rate = rs.init_dataframe(rate_list,1)

                    r = sim(rate, k, k+ph1)
                    r.ccy = ccy1+ccy2

                    r.param = param_list[i]

                    if ccy2 not in ["JPY", "HKD", "MXN", "TRY","ZAR"]:
                        r.total_profit = r.total_profit * 100

                    if max_profit < r.total_profit:
                        max_profit = r.total_profit
                        t1_max = rs.rci_term01
                        t2_max = rs.rci_term02
                        p_max = ProfitPips
                        l_max = LossCutPips
                        th1_max = RCIThreshold01
                    #log =ccy1+" "+ccy2+" "+"{:.4f}".format(r.total_profit)
                    #r.save(result_file)
                    #logger.info(log)

                #"if max_profit <0:
                #    continue

                rs.rci_term01 = t1_max
                rs.rci_term02 = t2_max

                RCIThreshold01 = th1_max

                ProfitPips =p_max
                LossCutPips = l_max
                PipsRatio = 1.0
                Spread = 0.5

                if ccy2 not in ["JPY", "HKD", "MXN", "TRY", "ZAR"]:
                    LossCutPips = LossCutPips/100
                    ProfitPips = ProfitPips/100
                    PipsRatio = PipsRatio/100
                    Spread = Spread /100

                rate_list = copy.deepcopy(rate_list_org)
                rate = rs.init_dataframe(rate_list,1)

                r = sim(rate, k+ph1, k+ph1+ph2)
                r.ccy = ccy1+ccy2
                r.param = [t1_max, t2_max, th1_max, p_max, l_max]

                if ccy2 not in ["JPY", "HKD", "MXN", "TRY","ZAR"]:
                    r.total_profit = r.total_profit * 100

                log =ccy1+" "+ccy2+" "+"{:.4f}".format(r.total_profit) + " "+str(t1_max)+" "+str(t2_max)+" " + str(th1_max)+ " "+ str(p_max)+ " " + str(l_max)

                logger.info(log)
