import numpy as np
import pandas as pd
import techlib as tl
import sys

SHORT_TERM = 12
LONG_TERM = 26
SIGNAL_TERM = 9

r = pd.read_csv('quote.csv',sep=',', \
    #names=('DATE','USD','GBP','EUR','CAD','CHF','AUD','NZD','ZAR'), \
    dtype=[('DATE','S10'), \
            ('USD','f8'),\
            ('GBP','f8'),\
            ('EUR','f8'),\
            ('CAD','f8'),\
            ('CHF','f8'),\
            ('AUD','f8'),\
            ('NZD','f8'),\
            ('ZAR','f8'),\
            ('ALL','f8'),\
            ('JPY','f8')], \
    parse_dates=True)

ccy1=sys.argv[1]
ccy2=sys.argv[2]
rate = pd.DataFrame( {'DATE': [], 'CLOSE':[], 'AVG':[], 'MIN':[], 'MAX':[] })
rate['CLOSE'] = r[ccy1]/r[ccy2]
rate['DATE'] = r['DATE']
rate['AVG'] = rate['CLOSE'].rolling(250).mean()
rate['MIN'] = rate['CLOSE'].rolling(20).mean()
rate['MAX'] = rate['CLOSE'].rolling(20).mean()


#rate = pd.read_csv('zarjpy.csv',sep=',', names=('DATE','CLOSE'), parse_dates=True)

#macd = pd.DataFrame({'MACD':iMA(rate, 12) - iMA(rate, 26)})
#signal = pd.DataFrame({'SIGNAL':iMA(macd,9,applied_price='MACD')})

macd = tl.iMA(rate, SHORT_TERM) - tl.iMA(rate, LONG_TERM)
macddf = pd.DataFrame({'MACD':macd})

signal = tl.iMA( macddf, SIGNAL_TERM, applied_price='MACD')

rsi = tl.rsi(rate, 14)
#signal = pd.DataFrame({'SIGNAL':iMA(macd,9,applied_price='MACD')})



macdset = pd.DataFrame({'MACD': macd, 'SIGNAL': signal, 'RSI':rsi})

def BuyEntry(i): # 買いシグナル
	if  macdset['MACD'][i-1] < macdset['SIGNAL'][i-1] \
   	and macdset['MACD'][i] > macdset['SIGNAL'][i]:
        
		return True
    

def SellEntry(i): # 売りシグナル
	if macdset['MACD'][i-1] > macdset['SIGNAL'][i-1] \
    and macdset['MACD'][i] < macdset['SIGNAL'][i]:
        
		return True
    
    

def BuyExit(i): # 買いポジション決済シグナル
    if SellEntry(i):
        return True
    
    if LongPos/rate['CLOSE'][i] >1.05:
        return True

    if LongPos/rate['CLOSE'][i] <0.997:
        return True


def SellExit(i): # 売りポジション決済シグナル
    if BuyEntry(i):
        return True

    if ShortPos/rate['CLOSE'][i] <0.95:
        return True

    if ShortPos/rate['CLOSE'][i] >1.003:
        return True

#LongPos = pd.Series(0.0, index=rate.index) # 買いポジション情報
#ShortPos = LongPos.copy() # 売りポジション情報
Lots = 1.0 # 売買ロット数

#LongPos = pd.DataFrame(colums['DATE','RATE','PROFIT'])
#ShortPos = pd.DataFrame()


LongPos = 0.0
ShortPos =0.0
TotalProfit =0.0
PROFIT = 0.0
for i in range( 60, len(rate)-1 ):
    if BuyExit(i) and LongPos != 0.0:
        PROFIT = rate['CLOSE'][i] - LongPos
        TotalProfit = TotalProfit + PROFIT
        print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], LongPos, PROFIT) )
        LongPos = 0.0


    if SellExit(i) and ShortPos != 0.0: 
        PROFIT = rate['CLOSE'][i] - ShortPos
        PROFIT = -PROFIT
        TotalProfit = TotalProfit + PROFIT
        print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
        ShortPos = 0.0
    
    if BuyEntry(i) and LongPos == 0.0: 
        LongPos = rate['CLOSE'][i]
        if ShortPos!= 0.0:
            PROFIT = rate['CLOSE'][i] - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT
            print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
            
    if SellEntry(i) and ShortPos == 0.0: 
        ShortPos = rate['CLOSE'][i]
        if LongPos!= 0.0:
            PROFIT =  rate['CLOSE'][i] - LongPos
            TotalProfit = TotalProfit + PROFIT
            print(" {} {:.2f} {:.2f} {:.2f}".format(rate['DATE'][i],rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0

    


print('total')
print(TotalProfit)
print(LongPos)
print(ShortPos)