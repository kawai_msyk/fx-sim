import numpy as np
import pandas as pd
import techlib as tl
import rateset
import result
import sys
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


LossCutPips = 5
ProfitPips = 45
PipsRatio = 1.0

Spread = 0.5
BuyFlag = False
SellFlag = False

BuyRate = 0.0
SellRate = 0.0
JudgeCnt = 0



def BuyEntry(i, rate, threshold): # 買いシグナル
    if rate['AVG'][i] + threshold*rate['STDEV'][i] < rate['CLOSE'][i]:
        global ProfitPips
        #ProfitPips = min(rate['STDEV'][i]*1000, 30)
        #ProfitPips = max(min(rate['STDEV'][i]*1000, 50), 10)
        #ProfitPips = ProfitPips * PipsRatio
        #if abs(( rate['MACD'][i] - rate['MACD'][i-1] )/(rate['SIGNAL'][i] - rate['SIGNAL'][i-1])) < 10.0:
        #    return False

        return True

    return False

def SellEntry(i, rate, threshold): # 売りシグナル
    if rate['AVG'][i] - threshold*rate['STDEV'][i] > rate['CLOSE'][i]:
        #global ProfitPips
        #ProfitPips = min(rate['STDEV'][i]*1000, 30)
        #ProfitPips = max(min(rate['STDEV'][i]*1000, 50), 10)
        #ProfitPips = ProfitPips * PipsRatio
        #if rate['MACD'][i] > 0.5:
        #if  abs(( rate['MACD'][i] - rate['MACD'][i-1] )/(rate['SIGNAL'][i] - rate['SIGNAL'][i-1])) < 10.0:
        #    return False
        return True

    return False

def BuyExit(i, p, rate, threshold): # 買いポジション決済シグナル
    #if rate['CLOSE'][i] - p < -LossCutPips*0.01:
    #    return True, p-LossCutPips*0.01

    if rate['LOW'][i] - p < -LossCutPips*0.01:
        return True, p - LossCutPips*0.01
    #
    if rate['HIGH'][i] - p - Spread*0.01 > ProfitPips*0.01:
        return True, p + ProfitPips*0.01

    #if rate['HIGH'][i] - p - Spread*0.01 > LossCutPips*0.01 and ShortPos!=0:
    #    return True, p + LossCutPips*0.01
    #if rate['CORR'][i] <0.7:
    #    return True, rate['CLOSE'][i]
    #return True, rate['CLOSE'][i]

    '''
    #print(ProfitPips)
    if SellEntry(i, rate):
        if rate['CLOSE'][i] > p:
            return True, rate['CLOSE'][i]
    '''
    #if SellEntry(i, rate):
    if rate['AVG'][i] + threshold*rate['STDEV'][i] > rate['CLOSE'][i]:
        return True, rate['CLOSE'][i]
    
    return False, 0.0

def SellExit(i, p, rate,threshold): # 売りポジション決済シグナル
    #if rate['CLOSE'][i] - p > LossCutPips*0.01:
    #    return True, p + LossCutPips*0.01

    if rate['HIGH'][i] - p > LossCutPips*0.01:
        return True, p + LossCutPips*0.01

    #
    if rate['LOW'][i] - p + Spread*0.01 < -ProfitPips*0.01:
        return True, p - ProfitPips*0.01
    
    #if rate['LOW'][i] - p + Spread*0.01 < -LossCutPips*0.01 and LongPos!=0:
    #    return True, p - LossCutPips*0.01

    #if rate['CORR'][i] <0.7:
    #    return True, rate['CLOSE'][i]
    #return True, rate['CLOSE'][i]
    
    
    
    '''    
    if BuyEntry(i, rate):
        if p > rate['CLOSE'][i]:
            return True, rate['CLOSE'][i]
    '''
    #if BuyEntry(i, rate):
    if rate['AVG'][i] - threshold*rate['STDEV'][i] < rate['CLOSE'][i]:
        return True, rate['CLOSE'][i]
    
    return False, 0.0

Lots = 1.0 # 売買ロット数


def sim(rate, open_threshold,close_threshold):
    TotalProfit =0.0
    PROFIT = 0.0
    NumWin = 0
    NumLoss = 0
    LongPos = 0.0
    ShortPos =0.0
    LongNum = 0
    ShortNum = 0
    MaxConsvWin =0
    ConsvWin = 0
    MaxConsvLoss = 0
    ConsvLoss = 0
    BuyFlag = False
    SellFlag = False


    for i in range(1, len(rate)-1 ):
        if LongPos != 0.0 and BuyExit(i, LongPos, rate, close_threshold)[0]:
            #PROFIT = rate['CLOSE'][i] - LongPos
            PROFIT = BuyExit(i, LongPos, rate, close_threshold)[1] - LongPos
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE BUY {} {:.2f} {:.2f}  {:.4f}".format(rate['DATE'][i], rate['CLOSE'][i], LongPos, PROFIT) )
            LongPos = 0.0
            LongNum = 0
            if PROFIT > 0.0:
                NumWin = NumWin + 1
                ConsvWin += 1
                if ConsvWin > MaxConsvWin:
                    MaxConsvWin = ConsvWin
                ConsvLoss = 0

            else:
                NumLoss = NumLoss + 1
                ConsvWin = 0
                ConsvLoss += 1
                if ConsvLoss > MaxConsvLoss:
                    MaxConsvLoss = ConsvLoss

        if ShortPos != 0.0  and  SellExit(i, ShortPos, rate, close_threshold)[0]: 
            PROFIT = SellExit(i, ShortPos, rate, close_threshold)[1]  - ShortPos
            PROFIT = -PROFIT
            TotalProfit = TotalProfit + PROFIT
            logger.debug("CLOSE SELL {} {:.2f} {:.2f} {:.4f}".format(rate['DATE'][i],rate['CLOSE'][i], ShortPos, PROFIT) )
            ShortPos = 0.0
            ShortNum = 0

            if PROFIT > 0.0:
                NumWin = NumWin + 1
                ConsvWin += 1
                if ConsvWin > MaxConsvWin:
                    MaxConsvWin = ConsvWin
                ConsvLoss = 0
            else:
                NumLoss = NumLoss + 1
                ConsvWin = 0
                ConsvLoss += 1
                if ConsvLoss > MaxConsvLoss:
                    MaxConsvLoss = ConsvLoss
                
                            
        if SellEntry(i, rate, open_threshold) and ShortNum == 0 : 
            ShortPos = rate['CLOSE'][i]
            ShortNum += 1

        if BuyEntry(i,rate, open_threshold) and LongNum==0: 
            LongPos = rate['CLOSE'][i]
            LongNum += 1
            BuyFlag = False            

    r  = result.Result()
    r.num_win = NumWin
    r.num_loss = NumLoss
    r.max_consv_win = MaxConsvWin
    r.max_consv_loss = MaxConsvLoss
    r.total_profit = TotalProfit

    #logger.info(rate['CLOSE'][i])
    logger.info(NumWin)
    logger.info(NumLoss)
    if NumWin != 0 and NumLoss != 0:
        logger.info(NumWin/(NumWin+NumLoss) )
    
    logger.info( MaxConsvWin)
    logger.info( MaxConsvLoss)
    logger.info(LongPos)
    logger.info(ShortPos)

    return r

if __name__ == '__main__':

    #r = rateset.getrate()
    #rate = rateset.initrate(r,"USD", "JPY")
    
    if len(sys.argv) > 1:
        ccy1=sys.argv[1]
        ccy2=sys.argv[2]
        ProfitPips = int(sys.argv[3])
        LossCutPips = int(sys.argv[4])
        Spread = float( sys.argv[5])
        if ccy2!='JPY':
            LossCutPips = LossCutPips/100
            ProfitPips = ProfitPips/100
            PipsRatio = PipsRatio/100
            Spread = Spread /100

        rs = rateset.RateSet()
        rate = rs.getrate1d(ccy1+ccy2,1)
        #rate = rateset.initrate(r)
        #rate = rateset.initrate(r,ccy1, ccy2)

        r = sim(rate)
        log =ccy1+" "+ccy2+" "+"{:.4f}".format(r.total_profit) 
        logger.info(log)

    else:    
        #CCY = ["JPY", "USD", "GBP", "EUR", "AUD", "NZD", "CAD", "CHF", "ZAR"]
        ccylist = []
        f = open('CCYLIST.txt', 'r')
        ccylist = f.readlines()
        f.close()
        term_list = []
        
        for i in range(10, 50, 2):
            for j in np.arange( 1.5, 3, 0.1):
                for k in np.arange( j-1.0, j,0.1):
                    #for p in range(100,1000,200):
                    #    for l in range(100,1000,200):
                    p=30000000
                    l=30000000
                    term_list.append([i,j,k,p,l])


        for i in range(len(term_list)):
            for j in range(len(ccylist)):
                
                macd_short_term = term_list[i][0]
                open_threshold =  round(term_list[i][1],2)
                close_threshold = round(term_list[i][2],2)
                
                #macd_long_term = term_list[i][1]
                #macd_signal_term = term_list[i][2]

                ccy1 = ccylist[j][0:3]
                ccy2 = ccylist[j][3:6]
                print(ccy1 + "/" + ccy2 )
                ProfitPips = 30000000
                LossCutPips = 1000000

                ProfitPips = term_list[i][3]
                LossCutPips = term_list[i][4]
                
                Spread = 0.5
                if ccy2 not in ["JPY", "HKD", "MXN", "TRY", "ZAR"]:
                    LossCutPips = LossCutPips/100
                    ProfitPips = ProfitPips/100
                    PipsRatio = PipsRatio/100
                    Spread = Spread /100
                
                rs = rateset.RateSet()
                #rs.set_macd_term(macd_short_term, macd_long_term, macd_signal_term)
                rs.macd_short_term = macd_short_term
                rs.open_threshold = open_threshold
                rs.close_threshold = close_threshold

                rate = rs.getrate1d(ccy1+ccy2,1)
                #rate = rs.getrate1h(ccy1+ccy2,1)

                r = sim(rate, open_threshold, close_threshold)
                r.ccy = ccy1+ccy2
                r.macd_short_term = macd_short_term
                #r.macd_long_term = macd_long_term
                #r.macd_signal_term = macd_signal_term

                r.open_threshold = open_threshold
                r.close_threshold = close_threshold

                r.target_profit = term_list[i][3]
                r.target_losscut = term_list[i][4]
                
                if ccy2 not in ["JPY", "HKD", "MXN", "TRY","ZAR"]:
                #if ccy2!='JPY':
                    r.total_profit = r.total_profit * 100

                log =ccy1+" "+ccy2+" "+"{:.4f}".format(r.total_profit) 
                
                    
                r.save("result.csv")
                logger.info(log)
        """

        for i in range(9):
            for j in range(9):
                LossCutPips = 30
                ccy1 = CCY[j]
                ccy2 = CCY[i]
                if ccy1 == ccy2:
                    continue
                if ccy1 == "GBP" or ccy2 == "GBP":
                    LossCutPips = 30
                if ccy2!='JPY':
                    LossCutPips = LossCutPips/100
                r, m, s = initrate(ccy1, ccy2)
                sim(r,m)
    """