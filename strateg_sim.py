import numpy as np
import pandas as pd
import techlib as tl
import sys
import logging
import glob
import mysql.connector as mydb
import csv
import json
import copy

class StrategSim:
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    self.algorithm = "macd15simple"
    self.rate_term = "8h"
    self.from_date = "20220101"
    self.to_date = "20221231"
    def __init__(self):
        self.conn = mydb.connect(
            host='127.0.0.1',
            port='3306',
            user='root',
            password='root',
            database='fx',
            auth_plugin='mysql_native_password'
        )

        # コネクションが切れた時に再接続してくれるよう設定
        self.conn.ping(reconnect=True)

        # 接続できているかどうか確認
        print(self.conn.is_connected())
        self.cur = self.conn.cursor()



    def get_candidate(self):
        sql = "SELECT ccy, short_term, long_term, signal_term, sum(profit) " \
            "FROM result_macd  where  algorithm = %s and sim_term = %s  and " \
            "from_date >= %s and to_date <=%s " \
            "group by ccy, short_term, long_term, signal_term " \
            "order by ccy "

        self.cur.execute(sql, (self.algorithm, self.rate_term, self.from_date, self.to_date,))
        rows =  self.cur.fetchall()
        r = []
        for row in rows:
            r.append( list(row) )

        return r

    def get_result(self, short_term, long_term, Signa_tert):
        sql = "SELECT ccy, short_term, long_term, signal_term, profit " \
            "FROM result_macd  where  algorithm = %s and sim_term = %s  and " \
            "from_date >= %s and to_date <=%s and " \
            "short_term = %s and long_term = %s and signal_term =%s"

        self.cur.execute(sql, (self.algorithm, self.rate_term, self.from_date, self.to_date, short_term, long_term, Signa_tert))
        rows =  self.cur.fetchall()
        r = []
        for row in rows:
            r.append( list(row) )

        return r

    def sim(self):

        candidate_list = self.get_candidate()

        for c in list:
            ccy = c[0]
            short_term = c[1]
            long_term = c[2]
            signal_term =  c[3]
            profit = c[4]
