import sys
import logging
import glob
import csv
import os
import mysql.connector as mydb
import traceback


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


path = ".//rate//1m//" # use your path
all_files = glob.glob(path + "202306*.txt")

conn = mydb.connect(
    host='127.0.0.1',
    port='3306',
    user='root',
    password='root',
    database='fx',
    auth_plugin='mysql_native_password'
)

# コネクションが切れた時に再接続してくれるよう設定
conn.ping(reconnect=True)

# 接続できているかどうか確認
print(conn.is_connected())
cur = conn.cursor()
def insert_files():
    li = []

    for filename in all_files:
        logger.debug(filename)
        insert(filename)



def insert(file):
    records = []
    with open(file) as f:
        for r in csv.reader(f):
            records.append(r)

    records.pop(0)
    #<TICKER>,<DTYYYYMMDD>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>
    try:
        cur.executemany("INSERT INTO rate1m VALUES (%s, %s, %s, %s, %s, %s, %s)", records)
    except Exception as e:
        t = traceback.format_exception_only(type(e), e)
        print(t)

        conn.rollback()

    conn.commit()


if __name__ == '__main__':
    r = insert_files()
