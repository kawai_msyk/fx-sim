import requests
import time
import csv
import sqlite3
import traceback
import datetime

def insert(records):
    conn = sqlite3.connect('fx.db')

    # カーソルを取得
    cur = conn.cursor()

    try:
        cur.executemany("INSERT INTO rate_raw(CCY, Date, Time, Time1m, Value) VALUES (?, ?, ?, ?, ?)", records)
    except Exception as e:
        t = traceback.format_exception_only(type(e), e)
        print(t)

        conn.rollback()

    conn.commit()


ccyset = set()
f = open('CCYLIST.txt', 'r')
ccylist = f.readlines()
f.close()

for r in ccylist:
    ccyset.add( r[0:6])

current_time = str(int(time.time() * 1000)) 
response = requests.get('https://www.click-sec.com/data/fxneo/rate/rate.csv?_='+ current_time)


print(response.status_code)    # HTTPのステータスコード取得

print(response.text)
rate_list = []
for row in csv.reader(response.text.strip().splitlines()):
    tmp_list = row[0:9] + row[9].split(" ")
    #tmp_list[9] = tmp_list[9].replace("/","")
    #tmp_list[10] = tmp_list[10].replace(":","")
    tmp_list[0] = tmp_list[0].replace("/","")

    if tmp_list[0] not in ccyset:
        continue
        
    rlist = []
    rlist.append(tmp_list[0])
    rlist.append(tmp_list[9].replace("/",""))
    rlist.append(tmp_list[10].replace(":",""))
    rlist.append(tmp_list[10].replace(":","")[0:4])
    rlist.append(tmp_list[1])

    rate_list.append(rlist)

print(rate_list)
insert(rate_list)



